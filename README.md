pkom
====

This is project that started as implementation of Signal protocol for my BSc thesis and
later was adapted to implement end-to-end encrypted group communication and to test few
things for my master's thesis. In the end I didn't have much use of it when writing
master's thesis, but maybe someone else will find it useful.

Please note, that code in this repo is mostly "private prototype"" quality, so definitely
don't use this as secure IM.

You will find here implementation of group instant messaging with end-to-end encryption. Network
protocol use QUIC as transport layer and MessagePack for encoding messages. Protocol mostly
follow request-response scheme, but it's possible for server to send new messages to clients
without "request" step. Cryptographic protocol for one-to-one messages is based on Signal
protocol. The one used for group messages is much simpler (or rather minimal) and doesn't
provide forward secrecy and post-compromise security.

