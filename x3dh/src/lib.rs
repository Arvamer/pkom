use curve25519_dalek::{
    constants::ED25519_BASEPOINT_TABLE, montgomery::MontgomeryPoint, scalar::Scalar,
};
use hkdf::Hkdf;
use rand::{CryptoRng, Rng};
use sha2::Sha512;

fn decode_scalar(mut scalar: [u8; 32]) -> Scalar {
    scalar[0] &= 248;
    scalar[31] &= 127;
    scalar[31] |= 64;

    Scalar::from_bits(scalar)
}

pub fn dh(k: [u8; 32], u: [u8; 32]) -> [u8; 32] {
    let k = decode_scalar(k);
    let u = MontgomeryPoint(u);

    (k * u).to_bytes()
}

pub fn gen_key(rng: &mut (impl Rng + CryptoRng)) -> [u8; 32] {
    let mut buf = [0u8; 32];
    rng.fill(&mut buf);

    buf
}

pub fn gen_pub_key(key: [u8; 32]) -> [u8; 32] {
    (&decode_scalar(key) * &ED25519_BASEPOINT_TABLE)
        .to_montgomery()
        .to_bytes()
}

pub fn x3dh_alice(
    ika: [u8; 32],
    ikb: [u8; 32],
    eka: [u8; 32],
    spkb: [u8; 32],
    opkb: Option<[u8; 32]>,
    info: &[u8],
) -> [u8; 32] {
    let mut ikm = [0xFFu8; 32 * 5];
    ikm[32..64].copy_from_slice(&dh(ika, spkb));
    ikm[64..96].copy_from_slice(&dh(eka, ikb));
    ikm[96..128].copy_from_slice(&dh(eka, spkb));

    if let Some(opkb) = opkb {
        ikm[128..160].copy_from_slice(&dh(eka, opkb));
        let mut okm = [0u8; 32];
        let hkdf = Hkdf::<Sha512>::new(None, &ikm[..]);
        hkdf.expand(info, &mut okm).unwrap();

        okm
    } else {
        let mut okm = [0u8; 32];
        let hkdf = Hkdf::<Sha512>::new(None, &ikm[..128]);
        hkdf.expand(info, &mut okm).unwrap();

        okm
    }
}

pub fn x3dh_bob(
    ika: [u8; 32],
    ikb: [u8; 32],
    eka: [u8; 32],
    spkb: [u8; 32],
    opkb: Option<[u8; 32]>,
    info: &[u8],
) -> [u8; 32] {
    let mut ikm = [0xFFu8; 32 * 5];
    ikm[32..64].copy_from_slice(&dh(spkb, ika));
    ikm[64..96].copy_from_slice(&dh(ikb, eka));
    ikm[96..128].copy_from_slice(&dh(spkb, eka));

    if let Some(opkb) = opkb {
        ikm[128..160].copy_from_slice(&dh(opkb, eka));
        let mut okm = [0u8; 32];
        let hkdf = Hkdf::<Sha512>::new(None, &ikm[..]);
        hkdf.expand(info, &mut okm).unwrap();

        okm
    } else {
        let mut okm = [0u8; 32];
        let hkdf = Hkdf::<Sha512>::new(None, &ikm[..128]);
        hkdf.expand(info, &mut okm).unwrap();

        okm
    }
}

#[cfg(test)]
mod tests {
    use rand::{rngs::StdRng, SeedableRng};

    #[test]
    fn dh() {
        let rng = &mut StdRng::seed_from_u64(0x0001_2345_6789_0adf);
        let ika_priv = super::gen_key(rng);
        let ika_pub = super::gen_pub_key(ika_priv);

        let ikb_priv = super::gen_key(rng);
        let ikb_pub = super::gen_pub_key(ikb_priv);

        let shared_a = super::dh(ika_priv, ikb_pub);
        let shared_b = super::dh(ikb_priv, ika_pub);

        assert_eq!(shared_a, shared_b)
    }

    #[test]
    fn x3dh() {
        let rng = &mut StdRng::seed_from_u64(0x0001_2345_6789_0adf);

        let ika_priv = super::gen_key(rng);
        let ika_pub = super::gen_pub_key(ika_priv);

        let eka_priv = super::gen_key(rng);
        let eka_pub = super::gen_pub_key(eka_priv);

        let ikb_priv = super::gen_key(rng);
        let ikb_pub = super::gen_pub_key(ikb_priv);

        let spkb_priv = super::gen_key(rng);
        let spkb_pub = super::gen_pub_key(spkb_priv);

        let opkb_priv = super::gen_key(rng);
        let opkb_pub = super::gen_pub_key(opkb_priv);

        let shared_a = super::x3dh_alice(
            ika_priv,
            ikb_pub,
            eka_priv,
            spkb_pub,
            Some(opkb_pub),
            b"test",
        );

        let shared_b = super::x3dh_bob(
            ika_pub,
            ikb_priv,
            eka_pub,
            spkb_priv,
            Some(opkb_priv),
            b"test",
        );

        assert_eq!(shared_a, shared_b);

        let shared_a = super::x3dh_alice(ika_priv, ikb_pub, eka_priv, spkb_pub, None, b"test");

        let shared_b = super::x3dh_bob(ika_pub, ikb_priv, eka_pub, spkb_priv, None, b"test");

        assert_eq!(shared_a, shared_b);
    }
}
