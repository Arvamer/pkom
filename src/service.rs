use std::{ops::Deref, sync::Arc};

use anyhow::{bail, format_err, Error, Result};
use bytes::Bytes;
use dashmap::DashMap;
use futures::{SinkExt, StreamExt};
use quinn::{RecvStream, SendStream};
use tracing::{error, info, info_span, instrument, trace};

use crate::db::{Db, SimpleGroup, User, Username};
use libpkom::{
    self, Frame, GroupId, KeyBundle, LoginResponse, MessageHeader, MessageKind, PkomWrite,
    SignedPublicKey, SimpleGroupMessageHeader, UserId,
};
use tracing_futures::Instrument;

#[derive(Clone)]
pub struct Context(Arc<ContextInternal>);

pub struct ContextInternal {
    connections: DashMap<UserId, PkomWrite<SendStream>>,
    db: Db,
    undelivered_msgs: DashMap<UserId, Vec<StoredMsg>>,
}

enum StoredMsg {
    PrivateMsg { header: Bytes, body: Bytes },
    SimpleGroupMsg { header: Bytes, body: Bytes },
}

impl Context {
    pub fn new() -> Result<Self> {
        Ok(Context(Arc::new(ContextInternal {
            connections: DashMap::new(),
            db: Db::new()?,
            undelivered_msgs: DashMap::new(),
        })))
    }
}

impl Deref for Context {
    type Target = ContextInternal;

    fn deref(&self) -> &Self::Target {
        self.0.deref()
    }
}

pub struct Service {
    uid: UserId,
    ctx: Context,
}

impl Service {
    #[instrument(skip(ctx, tx, rx, msg_tx))]
    pub async fn new(
        ctx: Context,
        tx: SendStream,
        rx: RecvStream,
        msg_tx: PkomWrite<SendStream>,
    ) -> Result<Self> {
        let (mut tx, mut rx) = libpkom::frame_parts(tx, rx);
        let uid = match rx.next().await {
            Some(Ok(msg)) => {
                match msg {
                    Frame::Login { username, password } => {
                        trace!("got login msg");
                        let login = Username::new(&username)?;

                        let uid = ctx.db.get_user_id(&login)?.ok_or_else(|| {
                            format_err!("There is no user with name {}", username)
                        })?;

                        let response = {
                            let user = ctx.db.get_user(&uid)?.unwrap();

                            if !user.check_password(&password) {
                                info!(%login, %uid, "incorrect password");
                                tx.send(&libpkom::failed()).await?;

                                bail!("incorrect password");
                            }

                            let response = LoginResponse { id: uid };

                            rmp_serde::to_vec(&response).unwrap()
                        };

                        ctx.connections.insert(uid, msg_tx);

                        info!(%login, %uid, "logged in");
                        tx.send(&libpkom::ok_with_data(response.into())).await?;

                        if let Some((_, msgs)) = ctx.undelivered_msgs.remove(&uid) {
                            // TODO: maybe spawn this?
                            let mut tx = ctx.connections.get_mut(&uid).unwrap();
                            for msg in msgs {
                                let frame = match msg {
                                    StoredMsg::PrivateMsg { header, body } => {
                                        libpkom::message(header, body)
                                    }
                                    StoredMsg::SimpleGroupMsg { header, body } => {
                                        libpkom::simple_group_message(header, body)
                                    }
                                };
                                tx.send(&frame).await?;
                            }
                        }

                        uid
                    }
                    Frame::AddUser {
                        username,
                        password,
                        identity,
                        onetime,
                        prekey,
                    } => {
                        let user = User::new(username, &password, *identity, *prekey);
                        let uid = UserId::random();
                        ctx.db.insert_user(uid, user)?;
                        ctx.db.insert_otks(&uid, &onetime)?;
                        let data = Bytes::copy_from_slice(uid.as_ref());
                        tx.send(&libpkom::ok_with_data(data)).await?;

                        uid
                    }
                    _ => bail!("Not a login msg"),
                }
            }
            e => bail!("Failed to get login msg: {:?}", e),
        };

        Ok(Service { uid, ctx })
    }

    pub async fn process(&self, tx: SendStream, rx: RecvStream) -> Result<()> {
        let (mut tx, mut rx) = libpkom::frame_parts(tx, rx);
        if let Some(msg) = rx.next().await {
            let span = info_span!("process request", request = ?msg);
            async {
                trace!("started processing event");
                match self.process_priv(msg?).await {
                    Ok(response) => {
                        let success =  !response.as_result().is_err();
                        info!(?response, success, "processing request finished");
                        tx.send(&response).await?;
                    }
                    Err(e) => {
                        if matches!(e.downcast_ref::<rmp_serde::decode::Error>(), Some(_)) {
                            info!(response = ?Frame::InvalidFrame, success = false, "processing request finished");
                            tx.send(&Frame::InvalidFrame).await?;
                        } else {
                            error!("failed to process request, err: {:#}", e);
                            tx.send(&Frame::InternalServerError).await?;
                        }
                    }
                }

                Ok::<(), Error>(())
            }
            .instrument(span)
            .await?;
        }

        Ok(())
    }

    async fn process_priv(&self, req: Frame) -> Result<Frame> {
        Ok(match req {
            Frame::AddUser { .. } | Frame::Login { .. } => {
                // This can be only handled as first message in session
                Frame::InvalidFrame
            }
            Frame::Ping => Frame::Ok,
            Frame::Message { header, body } => {
                let header_parsed: MessageHeader = rmp_serde::from_read_ref(&header)?;
                if header_parsed.from != self.uid {
                    return Ok(Frame::InvalidFrame);
                }
                match header_parsed.kind {
                    MessageKind::SimpleGroupInvitation(inv) => {
                        if let Some(group) = self.ctx.db.get_simple_group(&inv.gid)? {
                            if group.have_member(&self.uid) {
                                if self.ctx.db.get_user(&header_parsed.to)?.is_some() {
                                    self.ctx
                                        .db
                                        .add_simple_group_member(&inv.gid, &header_parsed.to)?;
                                } else {
                                    return Ok(Frame::NotFound);
                                }
                            } else {
                                return Ok(Frame::Forbidden);
                            }
                        } else {
                            // Return same error as when user is not in group
                            return Ok(Frame::Forbidden);
                        }
                    }
                    _ => (),
                }

                if let Some(mut msg_tx) = self.ctx.connections.get_mut(&header_parsed.to) {
                    trace!("sending msg");
                    // FIXME: Chnage ID for reciver
                    msg_tx.send(&libpkom::message(header, body)).await?;

                    Frame::Ok
                } else {
                    trace!("storing message for later");
                    // FIXME: check if this user exists
                    let msg = StoredMsg::PrivateMsg { header, body };
                    self.ctx
                        .undelivered_msgs
                        .entry(header_parsed.to)
                        .or_insert_with(Vec::new)
                        .push(msg);

                    Frame::Ok
                }
            }
            Frame::GetPubKey { uid: user_id } => {
                if let Some(user) = self.ctx.db.get_user(&user_id)? {
                    libpkom::ok_with_data(Bytes::from(user.identity().pub_key.to_vec()))
                } else {
                    Frame::NotFound
                }
            }
            Frame::GetPubKeyBundle { uid: user_id } => {
                if let Some(user) = self.ctx.db.get_user(&user_id)? {
                    let onetime = self.ctx.db.pop_otk(&user_id)?;

                    let identity = *user.identity();
                    let prekey = user.prekey();

                    let bundle = KeyBundle {
                        identity,
                        prekey: SignedPublicKey {
                            pub_key: prekey.pub_key,
                            pub_key_sig: prekey.pub_key_sig,
                        },
                        onetime,
                    };

                    libpkom::ok_with_data(rmp_serde::to_vec(&bundle).unwrap().into())
                } else {
                    Frame::NotFound
                }
            }
            Frame::GetUserId { username } => {
                let username = Username::new(&username)?;
                if let Some(uid) = self.ctx.db.get_user_id(&username)? {
                    libpkom::ok_with_data(uid.as_slice().to_vec().into())
                } else {
                    Frame::NotFound
                }
            }
            Frame::GetUsername { uid: user_id } => {
                if let Some(user) = self.ctx.db.get_user(&user_id)? {
                    libpkom::ok_with_data(user.display_name().as_bytes().to_vec().into())
                } else {
                    Frame::NotFound
                }
            }
            Frame::CreateSimpleGroup => {
                let gid = GroupId::random();
                self.ctx
                    .db
                    .insert_simple_group(&gid, &SimpleGroup::new(&self.uid))?;

                libpkom::ok_with_data(gid.as_ref().to_vec().into())
            }
            Frame::JoinSimpleGroup { gid } => {
                if !self.ctx.db.add_simple_group_member(&gid, &self.uid)? {
                    Frame::NotFound
                } else {
                    Frame::Ok
                }
            }
            Frame::SimpleGroupMessage { header, body } => {
                let h: SimpleGroupMessageHeader = rmp_serde::from_read_ref(&header)?;

                let group = match self.ctx.db.get_simple_group(&h.to)? {
                    Some(g) => g,
                    None => {
                        return Ok(Frame::NotFound);
                    }
                };

                if h.from != self.uid || !group.have_member(&self.uid) {
                    return Ok(Frame::Forbidden);
                }

                for uid in group.iter() {
                    if self.uid != uid {
                        if let Some(mut msg_tx) = self.ctx.connections.get_mut(&uid) {
                            msg_tx
                                .send(&libpkom::simple_group_message(header.clone(), body.clone()))
                                .await?;
                        } else {
                            let msg = StoredMsg::SimpleGroupMsg {
                                header: header.clone(),
                                body: body.clone(),
                            };
                            self.ctx
                                .undelivered_msgs
                                .entry(uid)
                                .or_insert_with(Vec::new)
                                .push(msg);
                        }
                    }
                }

                Frame::Ok
            }
            Frame::KickUserFromSimpleGroup { .. } => todo!(),
            Frame::ChangePrekey { .. } => todo!(),
            Frame::AddOnetimeKeys { .. } => todo!(),
            Frame::InternalServerError
            | Frame::InvalidFrame
            | Frame::Forbidden
            | Frame::Failed
            | Frame::Ok
            | Frame::NotFound
            | Frame::OkWithData { .. } => {
                // Client should not send these to us
                Frame::InvalidFrame
            }
        })
    }

    pub async fn finish(&self) {
        self.ctx.connections.remove(&self.uid);
        info!("User {:?} logged out", self.uid);
    }
}
