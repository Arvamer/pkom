use std::{fs, io};

use anyhow::{bail, Context, Result};
use quinn::Incoming;
use tracing::info;

use crate::{config::Config, utils};
use libpkom::ALPN_PKOM;
use std::sync::Arc;
use tokio::time::Duration;

pub fn create_endpoint(config: &Config) -> Result<Incoming> {
    let server_config = quinn::ServerConfig::default();
    let mut server_config = quinn::ServerConfigBuilder::new(server_config);
    server_config.protocols(&[ALPN_PKOM]);

    if config.keylog {
        server_config.enable_keylog();
    }

    if config.stateless_retry {
        server_config.use_stateless_retry(true);
    }

    let path = utils::get_data_dir()?;
    let cert_path = path.join("cert.der");
    let key_path = path.join("key.der");

    let (cert, key) = match fs::read(&cert_path).and_then(|x| Ok((x, fs::read(&key_path)?))) {
        Ok(x) => x,
        Err(ref e) if e.kind() == io::ErrorKind::NotFound => {
            info!("generating self-signed certificate");
            let cert = rcgen::generate_simple_self_signed(vec!["localhost".into()]).unwrap();
            let key = cert.serialize_private_key_der();
            let cert = cert.serialize_der().unwrap();
            fs::create_dir_all(&path).context("failed to create certificate directory")?;
            fs::write(&cert_path, &cert).context("failed to write certificate")?;
            fs::write(&key_path, &key).context("failed to write private key")?;

            (cert, key)
        }
        Err(e) => {
            bail!("failed to read certificate: {}", e);
        }
    };

    let key = quinn::PrivateKey::from_der(&key)?;
    let cert = quinn::Certificate::from_der(&cert)?;
    server_config.certificate(quinn::CertificateChain::from_certs(vec![cert]), key)?;

    let mut server_config = server_config.build();
    Arc::get_mut(&mut server_config.transport)
        .unwrap()
        .keep_alive_interval(Some(Duration::from_secs(5)));

    let mut endpoint = quinn::Endpoint::builder();
    endpoint.listen(server_config);

    let (endpoint, incoming) = endpoint.bind(&config.listen)?;
    info!("listening on {}", endpoint.local_addr()?);

    Ok(incoming)
}
