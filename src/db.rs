use std::{fmt, fs, num::NonZeroU32};

use anyhow::{format_err, Result};
use ring::pbkdf2;
use sled::{Db as Sled, IVec, Tree};
use unicode_normalization::UnicodeNormalization;

use crate::utils;
use libpkom::{GroupId, PublicKey, SignedPublicKey, UserId, EC_KEY_LENGTH};

const PBKDF2_ITERATIONS: NonZeroU32 = unsafe { NonZeroU32::new_unchecked(10_000) };
const SALT: [u8; 16] = [
    228, 248, 214, 119, 34, 42, 47, 144, 203, 114, 10, 86, 242, 186, 195, 97,
];
const PASSWORD_HASH_LEN: usize = 32;

fn concatenate_merge(
    _key: &[u8],
    old_value: Option<&[u8]>,
    merged_bytes: &[u8],
) -> Option<Vec<u8>> {
    let mut ret = old_value
        .map(|ov| {
            let mut v = Vec::with_capacity(ov.len() + merged_bytes.len());
            v.extend_from_slice(ov);

            v
        })
        .unwrap_or_default();

    ret.extend_from_slice(merged_bytes);

    Some(ret)
}

#[derive(Debug)]
pub struct Db {
    sled: Sled,
    /// user id -> user
    users: Tree,
    /// username -> user id
    usernames: Tree,
    /// user id -> onetime keys
    onetime_keys: Tree,
    /// group id -> simple group
    simple_groups: Tree,
}

impl Db {
    pub fn new() -> Result<Self> {
        let mut path = utils::get_data_dir()?;
        fs::create_dir_all(&path)?;
        path.push("db");
        let sled = sled::open(path)?;

        let users = sled.open_tree("users")?;
        let usernames = sled.open_tree("usernames")?;
        let onetime_keys = sled.open_tree("onetime_keys")?;
        onetime_keys.set_merge_operator(concatenate_merge);
        let simple_groups = sled.open_tree("simple_groups")?;
        simple_groups.set_merge_operator(concatenate_merge);

        Ok(Self {
            sled,
            users,
            usernames,
            onetime_keys,
            simple_groups,
        })
    }

    #[cfg(test)]
    pub fn new_in_memory() -> Result<Self> {
        use sled::Config;

        let sled = Config::new().temporary(true).open()?;

        let users = sled.open_tree("users")?;
        let usernames = sled.open_tree("usernames")?;
        let onetime_keys = sled.open_tree("onetime_keys")?;
        onetime_keys.set_merge_operator(concatenate_merge);
        let simple_groups = sled.open_tree("simple_groups")?;
        simple_groups.set_merge_operator(concatenate_merge);

        Ok(Self {
            sled,
            users,
            usernames,
            onetime_keys,
            simple_groups,
        })
    }

    pub fn get_user_id(&self, name: &Username) -> Result<Option<UserId>> {
        Ok(self
            .usernames
            .get(name)?
            .map(|v| UserId::from_slice(v.as_ref()).unwrap()))
    }

    pub fn get_user(&self, id: &UserId) -> Result<Option<User>> {
        Ok(self
            .users
            .get(id)?
            .map(|v| rmp_serde::from_read(v.as_ref()).unwrap()))
    }

    pub fn insert_user(&self, id: UserId, user: User) -> Result<()> {
        let username = Username::new(&user.display_name)?;

        if self.usernames.get(&username)?.is_some() {
            return Err(format_err!("Username {} already exists.", username));
        }

        self.usernames.insert(username, id.as_ref())?;

        self.users.insert(id, rmp_serde::to_vec(&user)?)?;

        Ok(())
    }

    pub fn pop_otk(&self, uid: &UserId) -> Result<Option<PublicKey>> {
        Ok(self
            .onetime_keys
            .fetch_and_update(uid, |opt| {
                opt.and_then(|x| {
                    if x.len() > EC_KEY_LENGTH {
                        Some(x[EC_KEY_LENGTH..].to_owned())
                    } else {
                        None
                    }
                })
            })?
            .map(|x| {
                let mut pub_key = [0u8; EC_KEY_LENGTH];
                pub_key.copy_from_slice(&x[..EC_KEY_LENGTH]);

                PublicKey { pub_key }
            }))
    }

    pub fn insert_otks(&self, uid: &UserId, otks: &[PublicKey]) -> Result<()> {
        let mut buf = Vec::with_capacity(otks.len() * EC_KEY_LENGTH);
        for otk in otks {
            buf.extend_from_slice(&otk.pub_key);
        }
        self.onetime_keys.merge(uid, buf)?;

        Ok(())
    }

    pub fn insert_simple_group(&self, gid: &GroupId, group: &SimpleGroup) -> Result<()> {
        self.simple_groups.insert(gid, &group.members)?;

        Ok(())
    }

    pub fn add_simple_group_member(&self, gid: &GroupId, uid: &UserId) -> Result<bool> {
        if !self.simple_groups.contains_key(&gid)? {
            return Ok(false);
        }
        self.simple_groups.merge(gid, uid)?;

        Ok(true)
    }

    pub fn get_simple_group(&self, gid: &GroupId) -> Result<Option<SimpleGroup>> {
        Ok(self
            .simple_groups
            .get(gid)?
            .map(|members| SimpleGroup { members }))
    }
}

#[derive(Clone, Ord, PartialOrd, Eq, PartialEq, Hash, Serialize, Deserialize, Debug)]
pub struct Username(String);

impl Username {
    pub fn new(s: &str) -> Result<Self> {
        let s = s.nfkc().collect::<String>().to_lowercase();
        let s = s.trim();

        if s.is_empty() {
            Err(format_err!("empty username"))
        } else if s.len() > 32 {
            Err(format_err!("username longer than 32 bytes"))
        } else if let Some(c) = s
            .chars()
            .find(|c| !(c.is_alphanumeric() || c.is_whitespace()))
        {
            Err(format_err!(
                "username with forbidden character: {} ({})",
                c,
                c.escape_unicode()
            ))
        } else {
            Ok(Username(s.to_owned()))
        }
    }
}

impl fmt::Display for Username {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl AsRef<[u8]> for Username {
    fn as_ref(&self) -> &[u8] {
        self.0.as_ref()
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct User {
    display_name: String,
    password: [u8; PASSWORD_HASH_LEN],
    identity: PublicKey,
    prekey: SignedPublicKey,
}

impl User {
    pub fn new(
        username: String,
        password: &[u8],
        identity: PublicKey,
        prekey: SignedPublicKey,
    ) -> Self {
        let mut salt = Vec::with_capacity(SALT.len() + username.as_bytes().len());
        salt.extend(&SALT);
        salt.extend(username.as_bytes());

        let mut hash = [0u8; PASSWORD_HASH_LEN];
        pbkdf2::derive(
            pbkdf2::PBKDF2_HMAC_SHA256,
            PBKDF2_ITERATIONS,
            &salt,
            password,
            &mut hash,
        );

        User {
            display_name: username,
            password: hash,
            identity,
            prekey,
        }
    }

    pub fn check_password(&self, password: &[u8]) -> bool {
        let mut salt = Vec::with_capacity(SALT.len() + self.display_name.as_bytes().len());
        salt.extend(&SALT);
        salt.extend(self.display_name.as_bytes());

        pbkdf2::verify(
            pbkdf2::PBKDF2_HMAC_SHA256,
            PBKDF2_ITERATIONS,
            &salt,
            password,
            &self.password,
        )
        .is_ok()
    }

    pub fn identity(&self) -> &PublicKey {
        &self.identity
    }

    pub fn prekey(&self) -> &SignedPublicKey {
        &self.prekey
    }

    pub fn display_name(&self) -> &str {
        &self.display_name
    }
}

// For now we can serialize as list of UserIds, when first UserId is leader
#[derive(Clone, PartialEq, Debug)]
pub struct SimpleGroup {
    // It's stored as u8, but they are only UserId (16 bytes each)
    // First user is leader and always must be present.
    members: IVec,
}

impl SimpleGroup {
    pub fn new(leader: &UserId) -> Self {
        Self {
            members: leader.as_ref().into(),
        }
    }

    pub fn have_member(&self, uid: &UserId) -> bool {
        self.members.chunks_exact(16).any(|c| c == uid.as_ref())
    }

    pub fn leader(&self) -> UserId {
        // There is at least leader in the group.
        UserId::from_slice(&self.members[..16]).unwrap()
    }

    pub fn iter<'a>(&'a self) -> impl Iterator<Item = UserId> + 'a {
        self.members
            .chunks_exact(16)
            .map(|c| UserId::from_slice(c).unwrap())
    }
}

impl AsRef<[u8]> for SimpleGroup {
    fn as_ref(&self) -> &[u8] {
        self.members.as_ref()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::convert::TryInto;

    #[test]
    fn otks_db() {
        let db = Db::new_in_memory().unwrap();
        let buf: Vec<u8> = (0..EC_KEY_LENGTH * 4)
            .map(|x| x.try_into().unwrap())
            .collect();
        let mut k = [0; EC_KEY_LENGTH];

        k.copy_from_slice(&buf[..EC_KEY_LENGTH]);
        let k1 = PublicKey { pub_key: k };
        k.copy_from_slice(&buf[EC_KEY_LENGTH..EC_KEY_LENGTH * 2]);
        let k2 = PublicKey { pub_key: k };
        k.copy_from_slice(&buf[EC_KEY_LENGTH * 2..EC_KEY_LENGTH * 3]);
        let k3 = PublicKey { pub_key: k };
        k.copy_from_slice(&buf[EC_KEY_LENGTH * 3..EC_KEY_LENGTH * 4]);
        let k4 = PublicKey { pub_key: k };

        let uuid = UserId::random();
        db.insert_otks(&uuid, &[k1, k2, k3, k4]).unwrap();

        let stored = db.onetime_keys.get(&uuid).unwrap().unwrap();
        assert_eq!(&stored, &buf);

        let otk = db.pop_otk(&uuid).unwrap().unwrap();
        assert_eq!(&otk, &k1);

        let otk = db.pop_otk(&uuid).unwrap().unwrap();
        assert_eq!(&otk, &k2);

        let otk = db.pop_otk(&uuid).unwrap().unwrap();
        assert_eq!(&otk, &k3);

        let otk = db.pop_otk(&uuid).unwrap().unwrap();
        assert_eq!(&otk, &k4);

        assert!(db.pop_otk(&uuid).unwrap().is_none());
    }

    #[test]
    fn groups() {
        let db = Db::new_in_memory().unwrap();

        let leader = UserId::random();
        let u1 = UserId::random();
        let u2 = UserId::random();
        let u3 = UserId::random();

        let gid = GroupId::random();
        let group = SimpleGroup::new(&leader);

        assert!(!db.add_simple_group_member(&gid, &u1).unwrap());

        db.insert_simple_group(&gid, &group).unwrap();

        let db_group = db.get_simple_group(&gid).unwrap();
        assert_eq!(&db_group, &Some(group));
        assert_eq!(db_group.as_ref().unwrap().leader(), leader);
        assert!(db_group.as_ref().unwrap().have_member(&leader));
        assert!(!db_group.as_ref().unwrap().have_member(&u1));

        assert!(db.add_simple_group_member(&gid, &u1).unwrap());
        assert!(db.add_simple_group_member(&gid, &u2).unwrap());

        let db_group = db.get_simple_group(&gid).unwrap().unwrap();
        assert_eq!(db_group.leader(), leader);
        assert!(db_group.have_member(&leader));
        assert!(db_group.have_member(&u1));
        assert!(db_group.have_member(&u2));
        assert!(!db_group.have_member(&u3));
    }
}
