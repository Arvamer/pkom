use anyhow::{format_err, Result};
use directories::ProjectDirs;
use std::path::PathBuf;

pub fn get_data_dir() -> Result<PathBuf> {
    Ok(ProjectDirs::from("", "", "pkom")
        .ok_or_else(|| format_err!("Failed to get project dir paths"))?
        .data_local_dir()
        .to_owned())
}
