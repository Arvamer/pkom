#[macro_use]
extern crate serde_derive;

use anyhow::{Context as _, Result};
use futures::{FutureExt, StreamExt, TryFutureExt};
use quinn::Connecting;
use structopt::StructOpt;
use tokio::signal;
use tracing::{error, info, instrument, trace};

use crate::{
    config::Config,
    service::{Context, Service},
};

mod config;
mod connection;
mod db;
mod service;
mod utils;

#[tokio::main]
async fn main() -> Result<()> {
    tracing_subscriber::fmt::init();

    futures::select! {
        r = run_server().fuse() => r?,
        _ = signal::ctrl_c().fuse() => (),
    }

    Ok(())
}

async fn run_server() -> Result<()> {
    let context = Context::new()?;
    let config = Config::from_args();
    let mut incoming = connection::create_endpoint(&config)?;

    while let Some(conn) = incoming.next().await {
        tokio::spawn(
            handle_connection(context.clone(), conn)
                .unwrap_or_else(move |e| error!("connection failed: {:#}", reason = e)),
        );
    }

    Ok(())
}

#[instrument(skip(ctx, conn))]
async fn handle_connection(ctx: Context, conn: Connecting) -> Result<()> {
    let quinn::NewConnection {
        connection,
        mut bi_streams,
        ..
    } = conn.await?;

    info!(conn.addr = %connection.remote_address(), "connection established");

    let (tx, rx) = match bi_streams.next().await {
        Some(Err(quinn::ConnectionError::ApplicationClosed { .. })) | None => {
            info!("connection closed");
            return Ok(());
        }
        Some(e @ Err(_)) => e.context("failed to get login stream")?,
        Some(Ok(s)) => s,
    };
    trace!("opened login channels");

    let msg_tx = libpkom::frame_write(connection.open_uni().await?);
    trace!("opened msg channel");

    let service = Service::new(ctx, tx, rx, msg_tx).await?;
    trace!("created service");

    while let Some(stream) = bi_streams.next().await {
        let (tx, rx) = match stream {
            Err(quinn::ConnectionError::ApplicationClosed { .. }) => {
                info!("connection closed");
                service.finish().await;

                return Ok(());
            }
            e @ Err(_) => {
                service.finish().await;

                e.context("failed to get next stream")?
            }
            Ok(s) => s,
        };
        trace!("new streams");

        service.process(tx, rx).await?;
    }

    // FIXME: will not be called if error is returned
    service.finish().await;

    Ok(())
}
