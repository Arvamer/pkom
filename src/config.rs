use std::net::SocketAddr;
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "pkom")]
pub struct Config {
    /// Enable NSS-compatible cryptographic key logging to the SSLKEYLOGFILE environment variable
    #[structopt(long = "keylog")]
    pub keylog: bool,
    /// Enable stateless retries
    #[structopt(long = "stateless-retry")]
    pub stateless_retry: bool,
    /// Address to listen on
    #[structopt(long = "listen", default_value = "[::1]:9876")]
    pub listen: SocketAddr,
}
