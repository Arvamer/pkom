use sodiumoxide::randombytes;
use std::{fmt, str::FromStr};
use uuid::Uuid;

#[derive(Debug, Copy, Clone, PartialEq, Eq, Default, Hash, Serialize, Deserialize)]
pub struct UserId(pub Uuid);

impl UserId {
    pub fn random() -> Self {
        let mut buf = [0u8; 16];
        randombytes::randombytes_into(&mut buf);

        UserId(Uuid::from_bytes(buf))
    }

    pub fn from_slice(buf: &[u8]) -> Result<Self, uuid::Error> {
        Ok(UserId(Uuid::from_slice(buf)?))
    }

    pub fn as_slice(&self) -> &[u8] {
        self.0.as_bytes() as &[_]
    }

    pub fn from_bytes(b: [u8; 16]) -> Self {
        Self(Uuid::from_bytes(b))
    }

    pub fn as_bytes(&self) -> &[u8; 16] {
        self.0.as_bytes()
    }
}

impl FromStr for UserId {
    type Err = uuid::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Self(Uuid::parse_str(s)?))
    }
}

impl AsRef<[u8]> for UserId {
    fn as_ref(&self) -> &[u8] {
        self.as_slice()
    }
}

impl fmt::Display for UserId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_fmt(format_args!("UserId({})", self.0.to_hyphenated_ref()))
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Default, Hash, Serialize, Deserialize)]
pub struct GroupId(pub Uuid);

impl GroupId {
    pub fn random() -> Self {
        let mut buf = [0u8; 16];
        randombytes::randombytes_into(&mut buf);

        GroupId(Uuid::from_bytes(buf))
    }

    pub fn from_slice(buf: &[u8]) -> Result<Self, uuid::Error> {
        Ok(GroupId(Uuid::from_slice(buf)?))
    }

    pub fn as_slice(&self) -> &[u8] {
        self.0.as_bytes() as &[_]
    }

    pub fn from_bytes(b: [u8; 16]) -> Self {
        Self(Uuid::from_bytes(b))
    }

    pub fn as_bytes(&self) -> &[u8; 16] {
        self.0.as_bytes()
    }
}

impl AsRef<[u8]> for GroupId {
    fn as_ref(&self) -> &[u8] {
        self.as_slice()
    }
}

impl FromStr for GroupId {
    type Err = uuid::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Self(Uuid::parse_str(s)?))
    }
}

impl fmt::Display for GroupId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_fmt(format_args!("GroupId({})", self.0.to_hyphenated_ref()))
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum Id {
    Group(GroupId),
    User(UserId),
}

impl Id {
    pub fn as_bytes(&self) -> &[u8; 16] {
        match self {
            Id::Group(GroupId(uuid)) | Id::User(UserId(uuid)) => uuid.as_bytes(),
        }
    }
}
