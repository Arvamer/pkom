#[macro_use]
extern crate serde_derive;

mod serde_helpers;
pub mod user;

use std::{io, str};

use futures::{Sink, Stream};
use pin_project::pin_project;
use rmp_serde::{decode::Error as DecodeError, encode::Error as EncodeError};
use sodiumoxide::crypto::secretbox::xsalsa20poly1305::MACBYTES;
use thiserror::Error;
use tokio::io::{AsyncRead, AsyncWrite};
use tokio_util::codec::{FramedRead, FramedWrite, LengthDelimitedCodec};

use bytes::Bytes;
use futures::task::{Context, Poll};
use serde::Deserialize;
use std::{
    fmt::{self, Debug, Formatter},
    pin::Pin,
    str::Utf8Error,
};

mod id;

pub use id::{GroupId, Id, UserId};

// Length of public and private key used for message encryption
pub const EC_KEY_LENGTH: usize = 32;
pub const MAC_LENGTH: usize = MACBYTES;

pub const ALPN_PKOM: &[u8] = b"pkom0";

#[derive(Clone, Serialize, Deserialize)]
pub struct SignedKeyPair {
    pub priv_key: [u8; EC_KEY_LENGTH],
    pub pub_key: [u8; EC_KEY_LENGTH],
    #[serde(with = "serde_helpers::BigArray")]
    pub pub_key_sig: [u8; 64],
}

impl SignedKeyPair {
    pub fn verify(&self, pub_key: [u8; 32]) -> Result<(), ()> {
        xeddsa::verify(pub_key, &self.pub_key, &self.pub_key_sig)
    }
}

impl Debug for SignedKeyPair {
    fn fmt(&self, f: &mut Formatter) -> Result<(), fmt::Error> {
        f.debug_struct("SignedKeyPair")
            .field("priv_key", &&self.priv_key[..])
            .field("pub_key", &&self.pub_key[..])
            .field("pub_key_sig", &&self.pub_key_sig[..])
            .finish()
    }
}

#[derive(Clone, Serialize, Deserialize)]
pub struct SignedPublicKey {
    pub pub_key: [u8; EC_KEY_LENGTH],
    #[serde(with = "serde_helpers::BigArray")]
    pub pub_key_sig: [u8; 64],
}

impl Debug for SignedPublicKey {
    fn fmt(&self, f: &mut Formatter) -> Result<(), fmt::Error> {
        f.debug_struct("SignedPublicKey")
            .field("pub_key", &&self.pub_key[..])
            .field("pub_key_sig", &&self.pub_key_sig[..])
            .finish()
    }
}

#[derive(Copy, Clone, Serialize, Deserialize, Eq, PartialEq)]
pub struct PublicKey {
    pub pub_key: [u8; EC_KEY_LENGTH],
}

impl Debug for PublicKey {
    fn fmt(&self, f: &mut Formatter) -> Result<(), fmt::Error> {
        f.debug_struct("PublicKey")
            .field("pub_key", &&self.pub_key[..])
            .finish()
    }
}

#[pin_project]
pub struct PkomRead<R: AsyncRead>(#[pin] FramedRead<R, LengthDelimitedCodec>);

impl<R: AsyncRead> Stream for PkomRead<R> {
    type Item = Result<Frame, PkomCodecError>;

    fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        match self.project().0.poll_next(cx) {
            Poll::Ready(Some(res)) => {
                Poll::Ready(Some(res.map_err(|e| e.into()).and_then(|bytes| {
                    rmp_serde::from_read(bytes.as_ref()).map_err(|e| e.into())
                })))
            }
            Poll::Ready(None) => Poll::Ready(None),
            Poll::Pending => Poll::Pending,
        }
    }
}

#[pin_project]
pub struct PkomWrite<W: AsyncWrite>(#[pin] FramedWrite<W, LengthDelimitedCodec>);

impl<W: AsyncWrite> Sink<&Frame> for PkomWrite<W> {
    type Error = PkomCodecError;

    fn poll_ready(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.project().0.poll_ready(cx).map_err(|e| e.into())
    }

    fn start_send(self: Pin<&mut Self>, item: &Frame) -> Result<(), Self::Error> {
        let item = rmp_serde::to_vec(&item)?;
        self.project().0.start_send(item.into())?;

        Ok(())
    }

    fn poll_flush(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.project().0.poll_flush(cx).map_err(|e| e.into())
    }

    fn poll_close(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.project().0.poll_close(cx).map_err(|e| e.into())
    }
}

#[derive(Error, Debug)]
pub enum PkomCodecError {
    #[error("io error")]
    CodecError(#[from] io::Error),
    #[error("serialization failed")]
    SerializationError(#[from] EncodeError),
    #[error("deserialization failed")]
    DeserializationError(#[from] DecodeError),
}

impl PkomCodecError {
    pub fn is_io(&self) -> bool {
        matches!(self, PkomCodecError::CodecError(_))
    }
}

pub fn frame_parts<R: AsyncRead, W: AsyncWrite>(
    writer: W,
    reader: R,
) -> (PkomWrite<W>, PkomRead<R>) {
    let builder = tokio_util::codec::length_delimited::Builder::new();

    (
        PkomWrite(builder.new_write(writer)),
        PkomRead(builder.new_read(reader)),
    )
}

pub fn frame_write<W: AsyncWrite>(writer: W) -> PkomWrite<W> {
    let builder = tokio_util::codec::length_delimited::Builder::new();

    PkomWrite(builder.new_write(writer))
}

pub fn frame_read<R: AsyncRead>(reader: R) -> PkomRead<R> {
    let builder = tokio_util::codec::length_delimited::Builder::new();

    PkomRead(builder.new_read(reader))
}

#[derive(Debug, Serialize, Deserialize)]
pub enum Frame {
    Login {
        username: String,
        #[serde(with = "serde_bytes")]
        password: Vec<u8>,
    },
    Message {
        // It's in serialized form, because it will be covered by AEAD, so we want to make sure,
        // that we not accidentally change serialized representation. See MessageHeader for details.
        header: Bytes,
        body: Bytes,
    },
    Ping,
    Ok,
    NotFound,
    Failed,
    InternalServerError,
    Forbidden,
    InvalidFrame,
    OkWithData {
        data: Bytes,
    },
    AddUser {
        username: String,
        #[serde(with = "serde_bytes")]
        password: Vec<u8>,
        identity: Box<PublicKey>,
        prekey: Box<SignedPublicKey>,
        onetime: Vec<PublicKey>,
    },
    GetPubKey {
        uid: UserId,
    },
    GetPubKeyBundle {
        uid: UserId,
    },
    GetUserId {
        username: String,
    },
    GetUsername {
        uid: UserId,
    },
    ChangePrekey {
        prekey: Box<SignedKeyPair>,
    },
    AddOnetimeKeys {
        keys: Vec<PublicKey>,
    },
    CreateSimpleGroup,
    JoinSimpleGroup {
        gid: GroupId,
    },
    KickUserFromSimpleGroup {
        gid: GroupId,
        uid: UserId,
    },
    SimpleGroupMessage {
        // It's in serialized form, because it will be covered by AEAD, so we want to make sure,
        // that we not accidentally change serialized representation. See SimpleGroupMessageHeader for details.
        header: Bytes,
        body: Bytes,
    },
}

impl Frame {
    /// Return FrameError if server responded with error
    pub fn err(self) -> Result<Self, FrameError> {
        self.as_result()?;

        Ok(self)
    }

    /// Check if server responded with error
    pub fn as_result(&self) -> Result<&Self, FrameError> {
        match self {
            Frame::Failed => Err(FrameError::Failed),
            Frame::NotFound => Err(FrameError::NotFound),
            Frame::InternalServerError => Err(FrameError::InternalServerError),
            Frame::Forbidden => Err(FrameError::Forbidden),
            Frame::InvalidFrame => Err(FrameError::InvalidFrame),
            _ => Ok(self),
        }
    }

    /// Returns deserialized data from `OkWithData` or error.
    pub fn response_data<'a, T: Deserialize<'a>>(&'a self) -> Result<T, FrameError> {
        match self.as_result()? {
            Frame::OkWithData { data } => Ok(rmp_serde::from_read_ref(data)?),
            _ => Err(FrameError::WrongMsg),
        }
    }

    /// Returns `&str` from `OkWithData` or error.
    pub fn response_text(&self) -> Result<&str, FrameError> {
        match self.as_result()? {
            Frame::OkWithData { data } => Ok(str::from_utf8(data)?),
            _ => Err(FrameError::WrongMsg),
        }
    }

    /// Consume `self` and return `String` from `OkWithData` or error.
    pub fn take_response_text(self) -> Result<String, FrameError> {
        match self.err()? {
            Frame::OkWithData { data } => {
                Ok(String::from_utf8(data.as_ref().to_owned()).map_err(|e| e.utf8_error())?)
            }
            _ => Err(FrameError::WrongMsg),
        }
    }

    /// Returns `UserId` from `OkWithData` or error.
    pub fn response_uid(&self) -> Result<UserId, FrameError> {
        match self.as_result()? {
            Frame::OkWithData { data } => Ok(UserId::from_slice(data)?),
            _ => Err(FrameError::WrongMsg),
        }
    }

    /// Returns `GroupId` from `OkWithData` or error.
    pub fn response_gid(&self) -> Result<GroupId, FrameError> {
        match self.as_result()? {
            Frame::OkWithData { data } => Ok(GroupId::from_slice(data)?),
            _ => Err(FrameError::WrongMsg),
        }
    }

    /// Returns `Ok(())` if `Frame::Ok` or error.
    pub fn ok(&self) -> Result<(), FrameError> {
        match self.as_result()? {
            Frame::Ok => Ok(()),
            _ => Err(FrameError::WrongMsg),
        }
    }
}

#[derive(Error, Debug)]
pub enum FrameError {
    #[error("server returned error")]
    Failed,
    #[error("requested resource was not found")]
    NotFound,
    #[error("server encountered unexpected error")]
    InternalServerError,
    #[error("user does not have necessary privileges to finish operation")]
    Forbidden,
    #[error("frame sent by client is not valid (in current context)")]
    InvalidFrame,
    #[error("got unexpected message")]
    WrongMsg,
    #[error("deserialization failed")]
    DeserializationError(#[from] DecodeError),
    #[error("string decoding failed")]
    Utf8Error(#[from] Utf8Error),
    #[error("UUID decoding failed")]
    UuidDecodeError(#[from] uuid::Error),
}

/// Header used in HeaderMsg::Message
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct MessageHeader {
    pub dh_pub: [u8; 32],
    pub pn: u64,
    pub n: u64,
    /// Seconds since 1970-01-01 00:00:00 UTC. This value is set by sender, and you should not
    /// trust it too much.
    pub timestamp: u64,
    /// Set by sender, should be validated by server.
    pub from: UserId,
    pub to: UserId,
    pub kind: MessageKind,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct LoginResponse {
    pub id: UserId,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct KeyBundle {
    pub identity: PublicKey,
    pub prekey: SignedPublicKey,
    pub onetime: Option<PublicKey>,
}

#[derive(Debug, Clone, Serialize, Deserialize, Eq, PartialEq)]
pub enum MessageKind {
    PlainText,
    InitialMessage(Box<InitialMessageHeader>),
    // I'm not sure if it's worth boxing (4/8 bytes vs 32 bytes), but as PlainText should be the
    // most common, let's box it for now.
    SimpleGroupInvitation(Box<SimpleGroupInvitationHeader>),
}

#[derive(Debug, Clone, Serialize, Deserialize, Eq, PartialEq)]
pub struct InitialMessageHeader {
    pub sender_pub_key: [u8; 32],
    pub receiver_pub_key: [u8; 32],
    pub ephemeral_key: [u8; 32],
    pub prekey: [u8; 32],
    pub onetime_prekey: Option<[u8; 32]>,
}

#[derive(Debug, Clone, Serialize, Deserialize, Eq, PartialEq)]
pub struct SimpleGroupInvitationHeader {
    pub gid: GroupId,
    pub leader: UserId,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SimpleGroupInvitation {
    pub base_key: [u8; 32],
    pub leader_pub_key: PublicKey,
}

/// Header used in Frame::SimpleGroupMessage
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SimpleGroupMessageHeader {
    /// Random nonce used for encryption
    pub nonce: [u8; 12],
    /// Seconds since 1970-01-01 00:00:00 UTC. This value is set by sender, and you should not
    /// trust it too much.
    pub timestamp: u64,
    /// Set by sender, should be validated by server.
    pub from: UserId,
    pub to: GroupId,
}

pub fn ok() -> Frame {
    Frame::Ok
}

pub fn ok_with_data(data: Bytes) -> Frame {
    Frame::OkWithData { data }
}

pub fn not_found() -> Frame {
    Frame::NotFound
}

pub fn failed() -> Frame {
    Frame::Failed
}

/// `keys` must have size of `NONCEBYTES + MACBYTES + EC_KEY_LENGTH * 2`
pub fn add_user(
    username: String,
    password: Vec<u8>,
    identity: Box<PublicKey>,
    prekey: Box<SignedPublicKey>,
    onetime: Vec<PublicKey>,
) -> Frame {
    Frame::AddUser {
        username,
        password,
        identity,
        prekey,
        onetime,
    }
}

pub fn login(username: String, password: Vec<u8>) -> Frame {
    Frame::Login { username, password }
}

pub fn get_pub_key_bundle(user: UserId) -> Frame {
    Frame::GetPubKeyBundle { uid: user }
}

pub fn get_user_id(username: String) -> Frame {
    Frame::GetUserId { username }
}

pub fn get_username(user_id: UserId) -> Frame {
    Frame::GetUsername { uid: user_id }
}

pub fn message(header: Bytes, body: Bytes) -> Frame {
    Frame::Message { header, body }
}

pub fn create_group() -> Frame {
    Frame::CreateSimpleGroup
}

pub fn simple_group_message(header: Bytes, body: Bytes) -> Frame {
    Frame::SimpleGroupMessage { header, body }
}

pub fn join_group(room: GroupId) -> Frame {
    Frame::JoinSimpleGroup { gid: room }
}
