use sodiumoxide::crypto::hash::sha256;

static SALT_RANDOM_BYTES: [u8; 8] = [99, 186, 40, 194, 114, 204, 164, 235];

pub fn salt(username: &[u8], password: &[u8], out: &mut [u8]) {
    assert!(out.len() >= 32);

    let username = sha256::hash(username);
    let password = sha256::hash(password);
    out[0..12].copy_from_slice(&password[0..12]);
    out[12..24].copy_from_slice(&username[0..12]);
    out[24..].copy_from_slice(&SALT_RANDOM_BYTES);
}
