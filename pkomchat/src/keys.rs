use std::{
    fs::{self, File},
    io::Write,
};

use anyhow::{anyhow, Context, Result};
use directories::ProjectDirs;
use rand::{CryptoRng, Rng};
use serde_derive::{Deserialize, Serialize};
use sodiumoxide::crypto::{
    hash::sha256,
    pwhash::scryptsalsa208sha256::{
        self as pwhash, MemLimit, OpsLimit, Salt, MEMLIMIT_INTERACTIVE, OPSLIMIT_INTERACTIVE,
    },
    secretbox::xsalsa20poly1305::{self as secretbox, Key, Nonce, NONCEBYTES},
};
use tracing::info;

use libpkom::{SignedKeyPair, UserId};

pub fn derive(username: &str, password: &[u8]) -> ([u8; 32], Vec<u8>) {
    let mut salt = Salt([0u8; 32]);
    libpkom::user::salt(username.as_bytes(), password, &mut salt.0);
    let salt2 = Salt(sha256::hash(&salt.0).0);

    let mut key_enc = [0u8; 32];
    let mut key_login = vec![0u8; 32];
    let ops_limit = OpsLimit(OPSLIMIT_INTERACTIVE.0 * 24);
    let mem_limit = MemLimit(MEMLIMIT_INTERACTIVE.0 * 16);

    println!("Deriving keys...");

    pwhash::derive_key(&mut key_enc, password, &salt, ops_limit, mem_limit).unwrap();
    pwhash::derive_key(&mut key_login, password, &salt2, ops_limit, mem_limit).unwrap();

    (key_enc, key_login)
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct KeyPair {
    pub pub_key: [u8; 32],
    pub priv_key: [u8; 32],
}

impl KeyPair {
    // pub fn open(encrypted: impl Into<EncryptedKeyPair>, key: &Key) -> Result<Self> {
    //     let encrypted = encrypted.into();
    //     let nonce = Nonce(encrypted.id);
    //     let decrypted = secretbox::open(&encrypted.priv_key, &nonce, key)
    //         .map_err(|_| anyhow!("Failed to decrypt key"))?;
    //     let mut priv_key = [0u8; 32];
    //     priv_key.copy_from_slice(&decrypted);

    //     Ok(Self {
    //         pub_key: encrypted.pub_key,
    //         priv_key,
    //     })
    // }

    pub fn new(rng: &mut (impl Rng + CryptoRng)) -> Self {
        let mut id = [0; 24];

        rng.fill(&mut id);
        let priv_key = x3dh::gen_key(rng);
        let pub_key = x3dh::gen_pub_key(priv_key);

        Self { pub_key, priv_key }
    }

    // pub fn encrypt(self, key: &Key) -> EncryptedKeyPair {
    //     let nonce = Nonce(self.id);
    //     let enc = secretbox::seal(&self.priv_key, &nonce, &key);
    //     let mut priv_key = [0u8; MACBYTES + 32];
    //     priv_key.copy_from_slice(&enc);

    //     EncryptedKeyPair {
    //         priv_key,
    //         pub_key: self.pub_key,
    //     }
    // }
}

pub fn sign_key_pair(
    keys: KeyPair,
    identity_key: &[u8; 32],
    rng: &mut (impl Rng + CryptoRng),
) -> SignedKeyPair {
    let mut sig = [0u8; 64];
    xeddsa::sign(identity_key, &keys.pub_key, rng, &mut sig);

    SignedKeyPair {
        priv_key: keys.priv_key,
        pub_key: keys.pub_key,
        pub_key_sig: sig,
    }
}

#[derive(Debug, Clone)]
pub struct Keys {
    pub enc_key: Key,
    pub user_id: UserId,
    pub identity_key: KeyPair,
    pub prekey: KeyPair,
    pub onetime_keys: Vec<KeyPair>,
}

impl Keys {
    pub fn open(enc_key: Key, user_id: UserId, username: &str) -> Result<Self> {
        let keys = StoredKeys::open(username, &enc_key)?;

        Ok(Keys {
            enc_key,
            user_id,
            identity_key: keys.identity_key,
            prekey: keys.prekey,
            onetime_keys: keys.onetime_keys,
        })
    }

    pub fn store(&self, username: &str) -> Result<()> {
        let keys = StoredKeysRef {
            identity_key: &self.identity_key,
            prekey: &self.prekey,
            onetime_keys: &self.onetime_keys,
        };
        keys.seal(username, &self.enc_key)?;

        Ok(())
    }

    pub fn log_pub_keys(&self) {
        let otks: Vec<_> = self.onetime_keys.iter().map(|k| &k.pub_key).collect();
        info!(id_key = ?self.identity_key.pub_key, prekey = ?self.prekey.pub_key, ?otks);
    }

    #[cfg(test)]
    pub fn new_test(rng: &mut (impl Rng + CryptoRng), u: u8) -> Self {
        let mut k1 = [0u8; 32];
        rng.fill(&mut k1);
        let mut uuid = [1; 16];
        uuid[15] = u;

        Keys {
            enc_key: Key(k1),
            user_id: UserId::from_slice(&uuid).unwrap(),
            identity_key: KeyPair::new(rng),
            prekey: KeyPair::new(rng),
            onetime_keys: vec![KeyPair::new(rng)],
        }
    }
}

#[derive(Deserialize)]
struct StoredKeys {
    pub identity_key: KeyPair,
    pub prekey: KeyPair,
    pub onetime_keys: Vec<KeyPair>,
}

impl StoredKeys {
    fn open(username: &str, key: &Key) -> Result<Self> {
        let mut path = ProjectDirs::from("", "", "pkomchat")
            .context("failed to get project dir paths")?
            .data_local_dir()
            .to_owned();
        path.push(username);
        fs::create_dir_all(&path)?;
        path.push("keys");

        let buf = fs::read(path)?;
        let nonce = Nonce::from_slice(&buf[..NONCEBYTES]).unwrap();
        let buf = secretbox::open(&buf[NONCEBYTES..], &nonce, key)
            .map_err(|()| anyhow!("decrypting keys failed"))?;

        Ok(rmp_serde::from_read_ref(&buf)?)
    }
}

#[derive(Serialize)]
struct StoredKeysRef<'a> {
    pub identity_key: &'a KeyPair,
    pub prekey: &'a KeyPair,
    pub onetime_keys: &'a Vec<KeyPair>,
}

impl<'a> StoredKeysRef<'a> {
    fn seal(&self, username: &str, key: &Key) -> Result<()> {
        let mut path = ProjectDirs::from("", "", "pkomchat")
            .context("failed to get project dir paths")?
            .data_local_dir()
            .to_owned();
        path.push(username);
        fs::create_dir_all(&path)?;
        path.push("keys");

        let buf = rmp_serde::to_vec(self)?;
        let nonce = secretbox::gen_nonce();
        let buf = secretbox::seal(&buf, &nonce, key);

        let mut file = File::create(path)?;
        file.write_all(nonce.as_ref())?;
        file.write_all(&buf)?;

        Ok(())
    }
}
