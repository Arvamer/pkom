#![feature(try_blocks)]

use std::{
    fs::{self, File},
    io::{self, Write},
    net::SocketAddr,
    panic,
    sync::{Arc, Mutex},
};

use ahash::AHashMap;
use anyhow::{bail, format_err, Context, Result};
use cursive::views::TextContent;
use directories::ProjectDirs;
use futures::StreamExt;
use quinn::{Connection, IncomingUniStreams, NewConnection};
use rand::{prelude::StdRng, SeedableRng};
use sodiumoxide::crypto::secretbox::xsalsa20poly1305::Key;
use structopt::StructOpt;
use tokio::{
    runtime::Handle,
    select,
    sync::{mpsc, oneshot, RwLock},
    task,
};
use tracing::{error, info, instrument, trace, warn};
use tracing_subscriber::EnvFilter;

use crate::{
    chat::{session::Session, Chat, ChatSession, IncomingMsg},
    keys::Keys,
    simple_group::SimpleGroupSession,
    tui::{Action, Contact, ListData},
};
use cursive::CbSink;
use dashmap::DashMap;
use keys::KeyPair;
use libpkom::{
    Frame, Id, LoginResponse, MessageHeader, MessageKind, PublicKey, SignedPublicKey,
    SimpleGroupMessageHeader, UserId, ALPN_PKOM,
};
use std::collections::hash_map::Entry;
use tokio::sync::mpsc::{UnboundedReceiver, UnboundedSender};

mod chat;
mod keys;
mod simple_group;
mod tui;
mod utils;

#[derive(StructOpt, Debug)]
#[structopt(name = "pkomchat")]
struct Opt {
    /// Perform NSS-compatible TLS key logging to the file specified in `SSLKEYLOGFILE`
    #[structopt(long)]
    keylog: bool,
    /// Trust local server certificate even if self-signed
    #[structopt(long, short = "T")]
    trust_local: bool,
    /// IP address to connect
    #[structopt(long, short, default_value = "[::1]:9876")]
    address: SocketAddr,
    /// Hostname used for TLS
    #[structopt(long, short, default_value = "localhost")]
    hostname: String,
    /// Username used to login
    #[structopt(long, short)]
    username: String,
    /// Password used to login
    #[structopt(long, short)]
    password: String,
    /// Create new account.
    #[structopt(long, short)]
    register: bool,
}

pub struct State {
    conn: Connection,
    keys: RwLock<Keys>,
    username: String,
    uid: UserId,
    action_tx: UnboundedSender<Action>,
    names: DashMap<UserId, String>,
}

impl State {
    fn new(
        conn: Connection,
        uid: UserId,
        keys: Keys,
        username: String,
        action_tx: UnboundedSender<Action>,
        names: DashMap<UserId, String>,
    ) -> Self {
        names.insert(uid, username.clone());

        Self {
            conn,
            keys: RwLock::new(keys),
            username,
            uid,
            action_tx,
            names,
        }
    }
}

#[derive(Clone)]
struct LogFile(Arc<Mutex<File>>);

impl Write for LogFile {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        self.0.lock().unwrap().write(buf)
    }

    fn flush(&mut self) -> io::Result<()> {
        self.0.lock().unwrap().flush()
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    let opt: Opt = Opt::from_args();
    let p = format!("pkomchat.{}.log", opt.username);
    let log_file = File::create(p)?;
    let log_file = LogFile(Arc::new(Mutex::new(log_file)));
    tracing_subscriber::fmt::fmt()
        .json()
        .with_writer(move || log_file.clone())
        .with_env_filter(EnvFilter::from_default_env())
        .init();

    trace!("trace level enabled");

    let mut endpoint = quinn::Endpoint::builder();
    let mut client_config = quinn::ClientConfigBuilder::default();
    client_config.protocols(&[ALPN_PKOM]);

    if opt.keylog {
        client_config.enable_keylog();
    }

    if opt.trust_local {
        let dirs = ProjectDirs::from("", "", "pkom")
            .ok_or_else(|| format_err!("Failed to get PKOM server project dir paths"))?;

        match fs::read(dirs.data_local_dir().join("cert.der")) {
            Ok(cert) => {
                client_config.add_certificate_authority(quinn::Certificate::from_der(&cert)?)?;
            }
            Err(ref e) if e.kind() == io::ErrorKind::NotFound => {
                warn!("local server certificate not found");
            }
            Err(e) => {
                error!("failed to open local server certificate: {}", e);
            }
        }
    }

    endpoint.default_client_config(client_config.build());
    let (endpoint, _) = endpoint.bind(&"[::]:0".parse().unwrap())?;

    info!(addr = %opt.address, hostname = %opt.hostname, "connecting...");

    let NewConnection {
        connection,
        uni_streams,
        ..
    } = endpoint
        .connect(&opt.address, &opt.hostname)?
        .await
        .context("failed to connect")?;

    info!(addr = %connection.remote_address(), "connected");

    let keys = if opt.register {
        create_account(opt.username.clone(), &opt.password, &connection)
            .await
            .context("failed to create account")?
    } else {
        login(opt.username.clone(), opt.password.into(), &connection)
            .await
            .context("failed to login")?
    };

    keys.log_pub_keys();

    let sessions = ChatSession::open_all(&keys.enc_key, &opt.username).await?;
    let group_sessions = SimpleGroupSession::open_all(&keys.enc_key.0, &opt.username).await?;

    let (action_tx, action_rx) = mpsc::unbounded_channel();

    let mut incoming_map_pm = AHashMap::new();
    let mut incoming_map_sg = AHashMap::new();
    let mut contacts = Vec::new();
    let names = DashMap::new();
    for session in sessions {
        let chat_content = TextContent::new("");
        contacts.push(Contact {
            chat: chat_content.clone(),
            name: session.name().to_owned(),
            id: session.id(),
        });
        names.insert(session.uid(), session.name().to_owned());
        incoming_map_pm.insert(session.id_bytes(), Chat::new(session, chat_content));
    }

    for session in group_sessions {
        let chat_content = TextContent::new("");
        contacts.push(Contact {
            chat: chat_content.clone(),
            name: session.name().to_owned(),
            id: session.id(),
        });
        incoming_map_sg.insert(session.id_bytes(), Chat::new(session, chat_content));
    }

    let state = Arc::new(State::new(
        connection,
        keys.user_id,
        keys,
        opt.username.clone(),
        action_tx,
        names,
    ));

    // Get channel for communicating with cursive
    let (sink_tx, sink_rx) = oneshot::channel();
    let s = state.clone();
    task::spawn(async move {
        utils::log_err(
            handle_incoming(
                uni_streams,
                incoming_map_pm,
                incoming_map_sg,
                s,
                action_rx,
                sink_rx,
            )
            .await,
        )
    });

    let s = state.clone();
    let r = task::spawn_blocking(move || tui::ui(contacts, sink_tx, Handle::current(), s)).await;

    match r {
        Err(e) if e.is_panic() => {
            let panic = e.into_panic();
            if let Some(s) = panic.downcast_ref::<String>() {
                error!("TUI panicked with {}", s);
            }
            panic::resume_unwind(panic);
        }
        _ => (),
    }

    state.conn.close(0u32.into(), b"");
    endpoint.wait_idle().await;

    Ok(())
}

async fn login(username: String, password: Vec<u8>, conn: &Connection) -> Result<Keys> {
    let login = username.clone();
    let (key_enc, key_login) =
        task::spawn_blocking(move || keys::derive(&login, &password)).await?;
    let key_enc = Key(key_enc);
    let login = libpkom::login(username.clone(), key_login);

    let msg = utils::send(conn, &login).await?;

    match msg {
        Frame::OkWithData { data } => {
            let response = rmp_serde::from_slice::<LoginResponse>(&data)?;

            task::spawn_blocking(move || Keys::open(key_enc, response.id, &username)).await?
        }
        Frame::Failed => {
            bail!("Invalid username or password");
        }
        _ => bail!("Unknown error in login"),
    }
}

pub async fn create_account(username: String, password: &str, conn: &Connection) -> Result<Keys> {
    let rng = &mut StdRng::from_entropy();
    let (key_enc, key_login) = keys::derive(&username, password.as_bytes());
    let key_enc = Key(key_enc);

    let identity_key = KeyPair::new(rng);
    let prekey = keys::sign_key_pair(KeyPair::new(rng), &identity_key.priv_key, rng);
    let onetime_keys: Vec<_> = (0..32).map(|_| KeyPair::new(rng)).collect();

    let frame = libpkom::add_user(
        username.clone(),
        key_login,
        Box::new(PublicKey {
            pub_key: identity_key.pub_key,
        }),
        Box::new(SignedPublicKey {
            pub_key: prekey.pub_key,
            pub_key_sig: prekey.pub_key_sig,
        }),
        onetime_keys
            .iter()
            .map(|x| PublicKey { pub_key: x.pub_key })
            .collect(),
    );

    let user_id = utils::send(conn, &frame).await?.response_uid()?;

    let keys = Keys {
        enc_key: key_enc,
        user_id,
        identity_key,
        prekey: KeyPair {
            pub_key: prekey.pub_key,
            priv_key: prekey.priv_key,
        },
        onetime_keys,
    };

    keys.store(&username)?;

    Ok(keys)
}

async fn handle_incoming(
    mut incoming: IncomingUniStreams,
    mut incoming_map_pm: AHashMap<[u8; 16], Chat<ChatSession>>,
    mut incoming_map_sg: AHashMap<[u8; 16], Chat<SimpleGroupSession>>,
    state: Arc<State>,
    mut action_rx: UnboundedReceiver<Action>,
    sink_rx: oneshot::Receiver<CbSink>,
) -> Result<()> {
    let cursive_tx = sink_rx.await?;

    let rx = loop {
        select! {
            rx = incoming.next() => {
                break rx.context("no incoming msg stream")??;
            }
            action = action_rx.recv() => {
                handle_action(&mut incoming_map_pm, &mut incoming_map_sg, &state, &cursive_tx, action).await?;
            }
        }
    };

    info!("Opened incoming stream");
    let mut rx = libpkom::frame_read(rx);

    loop {
        // FIXME: catch errors and display them in tui
        select! {
            frame = rx.next() => {
                if let Some(frame) = frame {
                    let frame = frame?;
                    process_incoming_frame(&mut incoming_map_pm, &mut incoming_map_sg, &state, &cursive_tx, frame).await?;
                } else {
                    break;
                }
            }
            action = action_rx.recv() => {
                handle_action(&mut incoming_map_pm, &mut incoming_map_sg, &state, &cursive_tx, action).await?;
            }
        }
    }

    Ok(())
}

async fn handle_action(
    pm_chat_map: &mut AHashMap<[u8; 16], Chat<ChatSession>>,
    sg_chat_map: &mut AHashMap<[u8; 16], Chat<SimpleGroupSession>>,
    state: &Arc<State>,
    cursive_tx: &CbSink,
    action: Option<Action>,
) -> Result<()> {
    match action {
        Some(Action::OpenedChatSession(session)) => {
            create_new_chat(session, pm_chat_map, &state, &cursive_tx).await?;
        }
        Some(Action::OpenedSimpleGroupSession(session)) => {
            create_new_chat(session, sg_chat_map, &state, &cursive_tx).await?;
        }
        Some(Action::InviteUser(uid, gid, response)) => {
            let (header, msg) = sg_chat_map
                .get(gid.as_bytes())
                .unwrap()
                .session()
                .prepare_invite_msg();
            let r = try {
                let body = rmp_serde::to_vec(&msg)?;
                pm_chat_map
                    .get_mut(uid.as_bytes())
                    .ok_or_else(|| {
                        format_err!(
                            "sending invitations to people outside your contacts are currently \
                             unsupported"
                        )
                    })?
                    .send_msg_with_kind(
                        &state,
                        body.as_slice().into(),
                        MessageKind::SimpleGroupInvitation(Box::new(header)),
                    )
                    .await?;
            };

            response.send(r).unwrap();
        }
        Some(Action::SendMsg(to, msg)) => match to {
            Id::Group(to) => {
                sg_chat_map
                    .get_mut(to.as_bytes())
                    .unwrap()
                    .send_text_msg(state.as_ref(), &msg)
                    .await?
            }
            Id::User(to) => {
                pm_chat_map
                    .get_mut(to.as_bytes())
                    .unwrap()
                    .send_text_msg(state.as_ref(), &msg)
                    .await?
            }
        },
        None => return Ok(()),
    }

    Ok(())
}

async fn create_new_chat<S>(
    session: S,
    chat_map: &mut AHashMap<[u8; 16], Chat<S>>,
    state: &Arc<State>,
    cursive_tx: &CbSink,
) -> Result<()>
where
    S: Session + Send + Sync + 'static,
    S::Header: Send,
{
    info!("Received new session ({})", session.name());
    session
        .save(
            &state.keys.read().await.enc_key.0,
            &state.username,
            &UserId::from_bytes(session.id_bytes())
                .0
                .to_hyphenated()
                .to_string(),
        )
        .await?;
    let id = session.id_bytes();
    let chat = create_chat(&cursive_tx, session);
    chat_map.insert(id, chat);

    cursive_tx
        .send(Box::new(|siv| {
            let idx = siv.user_data::<tui::Data>().unwrap().rooms_len() - 1;
            tui::change_room(siv, &ListData::Room(idx));
        }))
        .unwrap();

    Ok(())
}

#[instrument(skip(incoming_map_pm, incoming_map_sg, state, cursive_tx))]
async fn process_incoming_frame(
    incoming_map_pm: &mut AHashMap<[u8; 16], Chat<ChatSession>>,
    incoming_map_sg: &mut AHashMap<[u8; 16], Chat<SimpleGroupSession>>,
    state: &Arc<State>,
    cursive_tx: &CbSink,
    frame: Frame,
) -> Result<()> {
    match frame {
        Frame::Message {
            header: header_bytes,
            body,
        } => {
            info!("new msg frame");
            let header: MessageHeader = rmp_serde::from_slice(&header_bytes)?;
            let entry = incoming_map_pm.entry(*header.from.as_bytes());
            match entry {
                Entry::Vacant(e) => {
                    let init_msg = if let MessageKind::InitialMessage(header) = header.kind.clone()
                    {
                        Some(header)
                    } else {
                        None
                    };

                    let session =
                        ChatSession::create_session(&state, header.from, init_msg).await?;
                    let chat = create_chat(&cursive_tx, session);

                    e.insert(chat)
                        .recv_msg(
                            &state,
                            IncomingMsg {
                                header,
                                header_bytes,
                                body,
                            },
                        )
                        .await?
                }
                Entry::Occupied(mut e) => {
                    e.get_mut()
                        .recv_msg(
                            &state,
                            IncomingMsg {
                                header,
                                header_bytes,
                                body,
                            },
                        )
                        .await?
                }
            };
        }
        Frame::SimpleGroupMessage {
            header: header_bytes,
            body,
        } => {
            info!("new simple group msg frame");
            let header: SimpleGroupMessageHeader = rmp_serde::from_slice(&header_bytes)?;
            let entry = incoming_map_sg.entry(*header.to.as_bytes());
            match entry {
                Entry::Vacant(e) => {
                    let session = SimpleGroupSession::open(
                        &state.keys.read().await.enc_key.0,
                        &state.username,
                        &header.to.0.to_hyphenated().to_string(),
                    )
                    .await?;
                    let chat = create_chat(&cursive_tx, session);

                    e.insert(chat)
                        .recv_msg(
                            &state,
                            IncomingMsg {
                                header,
                                header_bytes,
                                body,
                            },
                        )
                        .await?
                }
                Entry::Occupied(mut e) => {
                    e.get_mut()
                        .recv_msg(
                            &state,
                            IncomingMsg {
                                header,
                                header_bytes,
                                body,
                            },
                        )
                        .await?
                }
            };
        }
        _ => warn!("Got unexpected msg"),
    }

    Ok(())
}

fn create_chat<S>(cursive_tx: &CbSink, session: S) -> Chat<S>
where
    S: Session + Send + Sync + 'static,
    S::Header: Send,
{
    let chat_content = TextContent::new("");

    let ct = chat_content.clone();
    let name = session.name().to_owned();
    let id = session.id();
    cursive_tx
        .send(Box::new(move |siv| {
            siv.user_data::<tui::Data>()
                .unwrap()
                .push_room(ct, &name, id);
        }))
        .unwrap();

    Chat::new(session, chat_content)
}
