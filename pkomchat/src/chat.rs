use std::{
    collections::HashMap,
    sync::Arc,
    time::{SystemTime, UNIX_EPOCH},
};

use anyhow::{bail, format_err, Context, Result};
use async_trait::async_trait;
use bytes::{Bytes, BytesMut};
use cursive::{
    theme::{Effect, Style},
    utils::markup::{markdown, StyledString},
    views::TextContent,
};
use data_encoding::BASE64;
use hkdf::Hkdf;
use hmac::{
    digest::generic_array::{typenum::U32, GenericArray},
    Hmac, Mac,
};
use libpkom::{
    Frame, Id, InitialMessageHeader, KeyBundle, MessageHeader, MessageKind, SimpleGroupInvitation,
    UserId,
};
use ring::aead::{self, Aad, LessSafeKey, UnboundKey, CHACHA20_POLY1305};
use serde_derive::{Deserialize, Serialize};
use sha2::{Sha256, Sha512};
use sodiumoxide::crypto::secretbox::xsalsa20poly1305::{self as secretbox, Key, Nonce};
use tokio::{
    fs,
    io::{AsyncReadExt, AsyncWriteExt},
    task,
};
use tracing::{info, instrument, trace};

use crate::{
    chat::{header::Header, session::Session},
    keys::KeyPair,
    simple_group::SimpleGroupSession,
    tui::Action,
    utils,
    utils::BufDisplay,
    State,
};
use std::ops::Deref;

pub mod header;
pub mod session;

#[derive(Debug)]
pub struct IncomingMsg<T> {
    pub header: T,
    pub header_bytes: Bytes,
    pub body: Bytes,
}

const MAX_SKIP: u64 = 1000;

pub async fn create_chat_session_from_uid(state: &State, with: UserId) -> Result<ChatSession> {
    if let Ok(session) = ChatSession::open(
        &state.keys.read().await.enc_key.0,
        &state.username,
        &with.0.to_hyphenated().to_string(),
    )
    .await
    {
        info!("Found existing session with {}", with);
        Ok(session)
    } else {
        info!("Initializing new session with {}", with);
        let ctx = "failed to get pub key bundle";
        let key_bundle = utils::send(&state.conn, &libpkom::get_pub_key_bundle(with))
            .await
            .context(ctx)?
            .response_data()
            .context(ctx)?;

        let ctx = "failed to get recipient username";
        let username = utils::send(&state.conn, &libpkom::get_username(with))
            .await
            .context(ctx)?
            .take_response_text()
            .context(ctx)?;

        // TODO: measure time to decide if should be marked as blocking
        ChatSession::new_alice(
            &state.keys.read().await.identity_key,
            key_bundle,
            username,
            with,
        )
    }
}

pub async fn create_chat_session_from_header<'a>(
    state: &'a State,
    header: &'a InitialMessageHeader,
    from: UserId,
) -> Result<ChatSession> {
    info!(
        "Creating new session from header {:?} with {}",
        header, from
    );
    let ctx = "failed to get addressee username";
    let username = utils::send(&state.conn, &libpkom::get_username(from))
        .await
        .context(ctx)?
        .take_response_text()
        .context(ctx)?;

    // TODO: measure time to decide if should be marked as blocking
    ChatSession::new_bob(&state, header, username, from).await
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ChatSession {
    uid: UserId,
    name: String,
    dh_tx: KeyPair,
    dh_rx: Option<[u8; 32]>,
    rk: [u8; 32],
    ck_tx: Option<[u8; 32]>,
    ck_rx: Option<[u8; 32]>,
    n_tx: u64,
    n_rx: u64,
    pn: u64,
    mk_skipped: HashMap<(Option<[u8; 32]>, u64), [u8; 32]>,
    initial_msg: Option<InitialMessageHeader>,
}

impl ChatSession {
    pub async fn create_session(
        state: &Arc<State>,
        chat_with: UserId,
        init_msg: Option<Box<InitialMessageHeader>>,
    ) -> Result<ChatSession> {
        Ok(if let Some(init_msg) = init_msg {
            let session = create_chat_session_from_header(&state, &init_msg, chat_with).await?;
            session
                .save(
                    &state.keys.read().await.enc_key.0,
                    &state.username,
                    &chat_with.0.to_hyphenated().to_string(),
                )
                .await?;

            session
        } else {
            create_chat_session_from_uid(&state, chat_with).await?
        })
    }

    pub async fn open_all(enc: &Key, username: &str) -> Result<Vec<Self>> {
        let mut path = utils::data_dir(username).await?;
        path.push("sessions");

        let mut v = Vec::new();
        fs::create_dir_all(&path).await?;
        let mut read_dir = fs::read_dir(path).await?;
        while let Some(e) = read_dir.next_entry().await? {
            if e.file_type().await?.is_file() {
                if let Some(uid) = e.file_name().to_str() {
                    v.push(Self::open(&enc.0, username, uid).await?);
                }
            }
        }

        Ok(v)
    }

    pub fn new_alice(
        id_key: &KeyPair,
        key_bundle: KeyBundle,
        name: String,
        uid: UserId,
    ) -> Result<Self> {
        xeddsa::verify(
            key_bundle.identity.pub_key,
            &key_bundle.prekey.pub_key,
            &key_bundle.prekey.pub_key_sig,
        )
        .map_err(|_| format_err!("Incorrect prekey signature"))?;

        let mut rng = rand::thread_rng();
        let ekey = x3dh::gen_key(&mut rng);

        let sk = x3dh::x3dh_alice(
            id_key.priv_key,
            key_bundle.identity.pub_key,
            ekey,
            key_bundle.prekey.pub_key,
            key_bundle.onetime.clone().map(|x| x.pub_key),
            b"PKOM INVITE DR",
        );

        let dh_tx = KeyPair::new(&mut rng);

        let hkdf = Hkdf::<Sha512>::new(
            Some(&sk),
            &x3dh::dh(dh_tx.priv_key, key_bundle.prekey.pub_key),
        );
        let mut hkdf_out = [0u8; 64];
        hkdf.expand(b"PKOM INVITE CHAIN INIT", &mut hkdf_out)
            .unwrap();
        let mut rk = [0u8; 32];
        let mut ck_tx = [0u8; 32];
        rk.copy_from_slice(&hkdf_out[..32]);
        ck_tx.copy_from_slice(&hkdf_out[32..]);

        info!(
            "{} identity key is {}. Verify it!",
            name,
            BASE64.encode(&key_bundle.identity.pub_key)
        );

        Ok(ChatSession {
            uid,
            name,
            dh_tx,
            dh_rx: Some(key_bundle.prekey.pub_key),
            rk,
            ck_tx: Some(ck_tx),
            ck_rx: None,
            n_tx: 0,
            n_rx: 0,
            pn: 0,
            mk_skipped: HashMap::new(),
            initial_msg: Some(InitialMessageHeader {
                sender_pub_key: id_key.pub_key,
                receiver_pub_key: key_bundle.identity.pub_key,
                ephemeral_key: x3dh::gen_pub_key(ekey),
                prekey: key_bundle.prekey.pub_key,
                onetime_prekey: key_bundle.onetime.map(|x| x.pub_key),
            }),
        })
    }

    #[instrument(skip(state))]
    pub async fn new_bob(
        state: &State,
        init_msg: &InitialMessageHeader,
        name: String,
        uid: UserId,
    ) -> Result<Self> {
        trace!("locking keys");
        let mut keys = state.keys.write().await;
        trace!("lock acquired");
        if keys.prekey.pub_key != init_msg.prekey {
            bail!("Invalid prekey ID");
        }
        let onetime = if let Some(onetime_prekey) = init_msg.onetime_prekey {
            let idx = keys
                .onetime_keys
                .iter()
                .position(|x| x.pub_key == onetime_prekey)
                .ok_or_else(|| format_err!("Invalid onetime key id"))?;
            let onetime = keys.onetime_keys.swap_remove(idx);

            let keys = keys.clone();
            let username = state.username.clone();
            task::spawn_blocking(move || keys.store(&username).unwrap());

            Some(onetime)
        } else {
            None
        };

        let sk = x3dh::x3dh_bob(
            init_msg.sender_pub_key,
            keys.identity_key.priv_key,
            init_msg.ephemeral_key,
            keys.prekey.priv_key,
            onetime.map(|x| x.priv_key),
            b"PKOM INVITE DR",
        );

        info!(
            "{} identity key is {}. Verify it!",
            name,
            BASE64.encode(&init_msg.sender_pub_key)
        );

        Ok(ChatSession {
            uid,
            name,
            dh_tx: keys.prekey.clone(),
            dh_rx: None,
            rk: sk,
            ck_tx: None,
            ck_rx: None,
            n_tx: 0,
            n_rx: 0,
            pn: 0,
            mk_skipped: HashMap::new(),
            initial_msg: None,
        })
    }

    // Inner Err is like None, but gives back reference
    fn try_skipped_msg_keys<'a>(
        &mut self,
        header_bytes: &[u8],
        nonce: [u8; 12],
        header: &MessageHeader,
        enc: &'a mut [u8],
    ) -> Result<std::result::Result<&'a mut [u8], &'a mut [u8]>> {
        if let Some(mk) = self.mk_skipped.remove(&(Some(header.dh_pub), header.n)) {
            let nonce = aead::Nonce::assume_unique_for_key(nonce);
            let key = UnboundKey::new(&CHACHA20_POLY1305, &mk).unwrap();

            Ok(Ok(LessSafeKey::new(key).open_in_place(
                nonce,
                Aad::from(header_bytes),
                enc,
            )?))
        } else {
            Ok(Err(enc))
        }
    }

    fn skip_msg_keys(&mut self, until: u64) -> Result<()> {
        if self.n_rx + MAX_SKIP < until {
            bail!("More than {} messages are missing!", MAX_SKIP);
        }

        if let Some(ck_rx) = self.ck_rx {
            while self.n_rx < until {
                let mut hmac = Hmac::<Sha256>::new_varkey(&ck_rx).unwrap();
                hmac.input(&[0x01]);
                let ck_rx = to_array_32(hmac.result_reset().code());
                hmac.input(&[0x02]);
                let mk = to_array_32(hmac.result_reset().code());

                self.ck_rx = Some(ck_rx);
                self.mk_skipped.insert((self.dh_rx, self.n_rx), mk);
                self.n_rx += 1;
            }
        }

        Ok(())
    }

    fn dh_ratchet(&mut self, header: &MessageHeader) {
        self.pn = self.n_tx;
        self.n_tx = 0;
        self.n_rx = 0;
        self.dh_rx = Some(header.dh_pub);

        let hkdf = Hkdf::<Sha512>::new(
            Some(&self.rk),
            &x3dh::dh(self.dh_tx.priv_key, self.dh_rx.unwrap()),
        );
        let mut hkdf_out = [0u8; 64];
        hkdf.expand(b"PKOM INVITE CHAIN INIT", &mut hkdf_out)
            .unwrap();
        self.rk.copy_from_slice(&hkdf_out[..32]);
        self.ck_rx
            .get_or_insert([0; 32])
            .copy_from_slice(&hkdf_out[32..]);

        self.dh_tx = KeyPair::new(&mut rand::thread_rng());

        let hkdf = Hkdf::<Sha512>::new(
            Some(&self.rk),
            &x3dh::dh(self.dh_tx.priv_key, self.dh_rx.unwrap()),
        );
        let mut hkdf_out = [0u8; 64];
        hkdf.expand(b"PKOM INVITE CHAIN INIT", &mut hkdf_out)
            .unwrap();
        self.rk.copy_from_slice(&hkdf_out[..32]);
        self.ck_tx
            .get_or_insert([0; 32])
            .copy_from_slice(&hkdf_out[32..]);
    }

    pub fn seal_msg_with_kind(
        &mut self,
        msg: BytesMut,
        from: UserId,
        kind: MessageKind,
    ) -> Result<Frame> {
        let header = rmp_serde::to_vec(&MessageHeader {
            dh_pub: self.dh_tx.pub_key,
            pn: self.pn,
            n: self.n_tx,
            timestamp: SystemTime::now()
                .duration_since(UNIX_EPOCH)
                .unwrap()
                .as_secs(),
            from,
            to: self.uid,
            kind,
        })
        .unwrap();

        self.seal_msg(msg, header.into())
    }

    pub fn uid(&self) -> UserId {
        self.uid
    }
}

#[async_trait]
impl Session for ChatSession {
    type Header = MessageHeader;
    const REQUIRE_SAVE_AFTER_ENC: bool = true;

    fn open_msg<'a>(
        &mut self,
        aad: &[u8],
        msg: &'a mut [u8],
        header: &Self::Header,
    ) -> Result<&'a mut [u8]> {
        trace!(to = ?self.name, "open msg");
        // Create copy of session, so if some operation fails, original will not be changed
        let mut session = self.clone();

        let mut nonce = [0u8; 12];
        nonce[..8].copy_from_slice(&header.n.to_le_bytes());

        match session.try_skipped_msg_keys(aad, nonce, &header, msg)? {
            Ok(dec) => {
                return Ok(dec);
            }
            Err(msg) => {
                let nonce = aead::Nonce::assume_unique_for_key(nonce);

                if Some(header.dh_pub) != session.dh_rx {
                    session.skip_msg_keys(header.pn)?;
                    session.dh_ratchet(&header);
                }

                session.skip_msg_keys(header.n)?;

                // TODO: use hmac from ring
                let mut hmac = Hmac::<Sha256>::new_varkey(
                    session.ck_rx.as_ref().expect("Missing rx key chain"),
                )
                .unwrap();
                hmac.input(&[0x01]);
                let ck_rx = to_array_32(hmac.result_reset().code());
                hmac.input(&[0x02]);
                let mk = to_array_32(hmac.result_reset().code());

                session.ck_rx = Some(ck_rx);
                session.n_rx += 1;

                let key = UnboundKey::new(&CHACHA20_POLY1305, &mk).unwrap();
                let msg = LessSafeKey::new(key).open_in_place(nonce, Aad::from(aad), msg)?;

                // No errors, Save changes
                *self = session;
                trace!(msg = %BufDisplay(&msg), to = ?self.name, "open msg finished");

                Ok(msg)
            }
        }
    }

    fn seal_standard_msg(&mut self, msg: BytesMut, from: UserId) -> Result<Frame> {
        let kind = if let Some(init_msg) = self.initial_msg.take() {
            MessageKind::InitialMessage(Box::new(init_msg))
        } else {
            MessageKind::PlainText
        };

        self.seal_msg_with_kind(msg, from, kind)
    }

    fn seal_msg(&mut self, mut msg: BytesMut, header: Bytes) -> Result<Frame> {
        trace!(msg = %BufDisplay(&msg), to = ?self.name, "seal msg");

        // TODO: use hmac from ring
        let mut hmac =
            Hmac::<Sha256>::new_varkey(self.ck_tx.as_ref().expect("Missing tx chain key")).unwrap();
        hmac.input(&[0x01]);
        let ck_tx = to_array_32(hmac.result_reset().code());
        hmac.input(&[0x02]);
        let mk = to_array_32(hmac.result_reset().code());

        debug_assert_ne!(mk, ck_tx);

        self.ck_tx = Some(ck_tx);

        let mut nonce = [0u8; 12];
        nonce[..8].copy_from_slice(&self.n_tx.to_le_bytes());
        let nonce = aead::Nonce::assume_unique_for_key(nonce);

        let key = UnboundKey::new(&CHACHA20_POLY1305, &mk).unwrap();
        LessSafeKey::new(key).seal_in_place_append_tag(nonce, Aad::from(&header), &mut msg)?;

        self.n_tx += 1;

        Ok(libpkom::message(header, msg.freeze()))
    }

    async fn open<'a>(
        enc_key: &'a [u8; 32],
        username: &'a str,
        session_name: &'a str,
    ) -> Result<Self> {
        let mut file = utils::open_file(username, "sessions", session_name).await?;
        let enc = Key(*enc_key);

        let mut buf = Vec::new();
        file.read_to_end(&mut buf).await?;

        // TODO: Use ring
        let nonce = Nonce::from_slice(&buf[(buf.len() - 24)..]).expect("Invalid nonce length");
        let buf = secretbox::open(&buf[..(buf.len() - 24)], &nonce, &enc)
            .map_err(|_| format_err!("Failed to open session"))?;

        Ok(rmp_serde::from_slice(&buf)?)
    }

    async fn save<'a>(
        &'a self,
        enc_key: &'a [u8; 32],
        username: &'a str,
        session_name: &'a str,
    ) -> Result<()> {
        trace!(to = ?self.name, "save session");

        // TODO: use ring
        let nonce = secretbox::gen_nonce();
        let serialized = rmp_serde::to_vec(self).expect("Serialization of session failed");
        let mut encrypted = secretbox::seal(&serialized, &nonce, &Key(*enc_key));
        encrypted.extend_from_slice(&nonce.0);

        let mut file = utils::create_file(username, "sessions", session_name).await?;
        file.write_all(&encrypted).await?;

        Ok(())
    }

    fn id(&self) -> Id {
        Id::User(self.uid)
    }

    fn name(&self) -> &str {
        &self.name
    }

    fn handle_special_msg(
        &mut self,
        state: &State,
        body: &[u8],
        header: &Self::Header,
    ) -> Result<bool> {
        Ok(match &header.kind {
            MessageKind::SimpleGroupInvitation(inv_header) => {
                let inv: SimpleGroupInvitation = rmp_serde::from_read_ref(body)?;
                let session = SimpleGroupSession::new(
                    inv_header.leader,
                    inv_header.gid,
                    inv.leader_pub_key,
                    inv.base_key,
                );
                state
                    .action_tx
                    .send(Action::OpenedSimpleGroupSession(session))
                    .unwrap();

                true
            }
            _ => false,
        })
    }
}

pub(crate) struct Chat<S: Session> {
    session: S,
    chat_text: TextContent,
}

impl<S: Session> Chat<S> {
    pub fn new(session: S, chat_text: TextContent) -> Self {
        Chat { session, chat_text }
    }

    pub fn session(&self) -> &S {
        &self.session
    }

    pub async fn send_text_msg(&mut self, state: &State, msg: &str) -> Result<()> {
        let frame = self
            .session
            .seal_standard_msg(msg.as_bytes().into(), state.uid)?;

        // FIXME: first save to backup file, then send message, then swap files
        utils::send(&state.conn, &frame)
            .await
            .with_context(|| format_err!("failed to send msg to {}", self.session.name()))?;

        if S::REQUIRE_SAVE_AFTER_ENC {
            let session_name = UserId::from_bytes(self.session.id_bytes())
                .0
                .to_hyphenated()
                .to_string();
            self.session
                .save(
                    &state.keys.read().await.enc_key.0,
                    &state.username,
                    &session_name,
                )
                .await
                .context("Failed to save session state")?;
        }

        Ok(())
    }

    pub async fn recv_msg(&mut self, state: &State, msg: IncomingMsg<S::Header>) -> Result<()>
    where
        S::Header: Header,
    {
        let mut body = msg.body.as_ref().to_vec();
        let m = self
            .session
            .open_msg(&msg.header_bytes, &mut body, &msg.header)
            .context("failed to decrypt incoming message")?;

        if !self.session.handle_special_msg(&state, &m, &msg.header)? {
            let text = std::str::from_utf8(m)?;
            let mut s = StyledString::plain("");

            // Make sure that we don't accidentally hold lock across await
            let from = msg.header.from();
            let name = if let Some(name) = state.names.get(&from).map(|x| x.deref().to_owned()) {
                name
            } else {
                let name = utils::send(&state.conn, &libpkom::get_username(from))
                    .await?
                    .take_response_text()?;
                state.names.insert(from, name.clone());

                name
            };

            s.append_styled(name, Style::from(Effect::Bold));
            s.append_styled(": ", Style::from(Effect::Bold));
            s.append(markdown::parse(text.trim()));
            s.append_plain("\n");

            self.chat_text.append(s);
        }

        if S::REQUIRE_SAVE_AFTER_ENC {
            let session_name = UserId::from_bytes(self.session.id_bytes())
                .0
                .to_hyphenated()
                .to_string();
            self.session
                .save(
                    &state.keys.read().await.enc_key.0,
                    &state.username,
                    &session_name,
                )
                .await
                .context("Failed to save session state")?;
        }

        Ok(())
    }
}

impl Chat<ChatSession> {
    pub async fn send_msg_with_kind(
        &mut self,
        state: &State,
        msg: &[u8],
        kind: MessageKind,
    ) -> Result<()> {
        let frame = self
            .session
            .seal_msg_with_kind(msg.into(), state.uid, kind)?;

        // FIXME: first save to backup file, then send message, then swap files
        utils::send(&state.conn, &frame)
            .await
            .with_context(|| format_err!("failed to send msg to {}", self.session.name()))?;

        if ChatSession::REQUIRE_SAVE_AFTER_ENC {
            let session_name = UserId::from_bytes(self.session.id_bytes())
                .0
                .to_hyphenated()
                .to_string();
            self.session
                .save(
                    &state.keys.read().await.enc_key.0,
                    &state.username,
                    &session_name,
                )
                .await
                .context("Failed to save session state")?;
        }

        Ok(())
    }
}

fn to_array_32(a: GenericArray<u8, U32>) -> [u8; 32] {
    unsafe { std::mem::transmute(a) }
}

#[cfg(test)]
mod test {
    // use crate::{
    //     chat::ChatSession,
    //     keys::{self, Keys},
    // };
    // use libpkom::{KeyBundle, PublicKey, SignedPublicKey, UserId};
    // use rand::{rngs::StdRng, SeedableRng};

    // #[test]
    // fn chat_session() {
    //     let rng = &mut StdRng::seed_from_u64(0x2832864823648);
    //     let keys_alice = Keys::new_test(rng, 0);
    //     let keys_eugeo = Keys::new_test(rng, 1);

    //     let mut uuid_alice = [1; 16];
    //     uuid_alice[15] = 0;
    //     let uuid_alice = UserId::from_bytes(&uuid_alice).unwrap();

    //     let mut uuid_eugeo = [1; 16];
    //     uuid_eugeo[15] = 1;
    //     let uuid_eugeo = UserId::from_bytes(&uuid_eugeo).unwrap();

    //     let signed = keys::sign_key_pair(
    //         keys_eugeo.prekey.clone().encrypt(&keys_eugeo.enc_key),
    //         &keys_eugeo.identity_key.priv_key,
    //         rng,
    //     );
    //     let onetime = keys_eugeo.onetime_keys[0].clone();

    //     let bundle_eugeo = KeyBundle {
    //         identity: PublicKey { pub_key: keys_eugeo.identity_key.pub_key },
    //         prekey: SignedPublicKey {
    //             pub_key: signed.pub_key,
    //             pub_key_sig: signed.pub_key_sig,
    //         },
    //         onetime: Some(PublicKey {
    //             pub_key: onetime.pub_key,
    //         }),
    //     };

    //     let mut alice_session =
    //         ChatSession::new_alice(&keys_alice, bundle_eugeo, "ユージオ".into(), uuid_eugeo)
    //             .unwrap();
    //     let mut eugeo_session = ChatSession::new_bob(
    //         &keys_eugeo,
    //         alice_session.initial_msg.as_ref().unwrap(),
    //         "アリス".into(),
    //         uuid_alice,
    //     )
    //     .unwrap();

    //     let msg1 = "——————————————".as_bytes();
    //     let (header_bytes, msg) = alice_session.seal_next(uuid_alice, uuid_eugeo, msg1);
    //     let header = rmp_serde::from_slice(&header_bytes).unwrap();
    //     let msg1_recv = eugeo_session.open(&header_bytes, &header, &msg).unwrap();
    //     assert_eq!(msg1_recv.as_slice(), msg1);

    //     let msg2 = "アリス！".as_bytes();
    //     let (header_bytes, msg) = eugeo_session.seal_next(uuid_alice, uuid_eugeo, msg2);
    //     let header = rmp_serde::from_slice(&header_bytes).unwrap();
    //     let msg2_recv = alice_session.open(&header_bytes, &header, &msg).unwrap();
    //     assert_eq!(msg2_recv.as_slice(), msg2);

    //     let msg3 = "！！！".as_bytes();
    //     let (header_bytes, msg) = eugeo_session.seal_next(uuid_alice, uuid_eugeo, msg3);
    //     let header = rmp_serde::from_slice(&header_bytes).unwrap();
    //     let msg3_recv = alice_session.open(&header_bytes, &header, &msg).unwrap();
    //     assert_eq!(msg3_recv.as_slice(), msg3);

    //     let msg4 = "ユージオ？".as_bytes();
    //     let (header_bytes, msg) = alice_session.seal_next(uuid_alice, uuid_eugeo, msg4);
    //     let header = rmp_serde::from_slice(&header_bytes).unwrap();
    //     let msg4_recv = eugeo_session.open(&header_bytes, &header, &msg).unwrap();
    //     assert_eq!(msg4_recv.as_slice(), msg4);
    // }
}
