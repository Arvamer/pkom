use std::{fmt, path::PathBuf};

use anyhow::{Context, Result};
use directories::ProjectDirs;
use futures::{SinkExt, StreamExt};
use quinn::Connection;
use tokio::fs::{self, File};
use tracing::{error, info, instrument, trace};

use libpkom::Frame;
use std::path::Path;

#[instrument(skip(conn))]
pub async fn send(conn: &Connection, frame: &Frame) -> Result<Frame> {
    trace!("started sending");
    let (tx, rx) = conn.open_bi().await?;
    trace!("opened channel");
    let (mut tx, mut rx) = libpkom::frame_parts(tx, rx);
    tx.send(frame).await?;
    trace!("finished sending");

    let res = rx.next().await.context("no response")??;
    info!("response {:?}", res);

    Ok(res)
}

pub async fn data_dir(user: &str) -> Result<PathBuf> {
    let mut path = ProjectDirs::from("", "", "pkomchat")
        .context("failed to get project dir paths")?
        .data_local_dir()
        .to_owned();
    path.push(user);
    fs::create_dir_all(&path).await?;

    Ok(path)
}

pub async fn open_file(
    user: &str,
    dir: impl AsRef<Path>,
    filename: impl AsRef<Path>,
) -> Result<File> {
    let mut path = data_dir(user).await?;
    path.push(dir);
    fs::create_dir_all(&path).await?;

    path.push(filename);
    let file = File::open(path).await?;

    Ok(file)
}

pub async fn create_file(
    user: &str,
    dir: impl AsRef<Path>,
    filename: impl AsRef<Path>,
) -> Result<File> {
    let mut path = data_dir(user).await?;
    path.push(dir);
    fs::create_dir_all(&path).await?;

    path.push(filename);
    let file = File::create(path).await?;

    Ok(file)
}

pub struct BufDisplay<T>(pub T);

impl<T> fmt::Display for BufDisplay<T>
where
    T: AsRef<[u8]>,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // Implementation from bytes crate
        write!(f, "b\"")?;
        for &b in self.0.as_ref() {
            // https://doc.rust-lang.org/reference/tokens.html#byte-escapes
            if b == b'\n' {
                write!(f, "\\n")?;
            } else if b == b'\r' {
                write!(f, "\\r")?;
            } else if b == b'\t' {
                write!(f, "\\t")?;
            } else if b == b'\\' || b == b'"' {
                write!(f, "\\{}", b as char)?;
            } else if b == b'\0' {
                write!(f, "\\0")?;
            // ASCII printable
            } else if b >= 0x20 && b < 0x7f {
                write!(f, "{}", b as char)?;
            } else {
                write!(f, "\\x{:02x}", b)?;
            }
        }
        write!(f, "\"")?;
        Ok(())
    }
}

pub fn log_err<T>(r: Result<T>) -> Result<T> {
    if let Err(err) = &r {
        error!(backtrace = %err.backtrace(), err = format_args!("{:#}", err));
    }

    r
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn buf_display() {
        let b = b"abc\xff";
        assert_eq!(format!("{}", BufDisplay(b)), r#"b"abc\xff""#);

        let b = "猫猫";
        assert_eq!(
            format!("{}", BufDisplay(b)),
            r#"b"\xe7\x8c\xab\xe7\x8c\xab""#
        );

        let b = vec![1u8, 2, 3];
        assert_eq!(format!("{}", BufDisplay(b)), r#"b"\x01\x02\x03""#);
    }
}
