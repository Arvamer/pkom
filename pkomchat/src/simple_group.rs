use anyhow::Result;
use async_trait::async_trait;
use bytes::{Bytes, BytesMut};
use libpkom::{
    Frame, GroupId, Id, PublicKey, SimpleGroupInvitation, SimpleGroupInvitationHeader,
    SimpleGroupMessageHeader, UserId,
};
use petname::Petnames;
use rand_pcg::Pcg64Mcg;
use ring::{
    aead::{Aad, LessSafeKey, Nonce, UnboundKey, CHACHA20_POLY1305},
    hkdf::{Salt, HKDF_SHA256},
};
use serde::{Deserialize, Serialize};
use std::time::{SystemTime, UNIX_EPOCH};
use tokio::{
    fs,
    io::{AsyncReadExt, AsyncWriteExt},
};

use crate::{chat::session::Session, keys::KeyPair, utils};

#[derive(Serialize, Deserialize, Debug)]
pub struct SimpleGroupSession {
    gid: GroupId,
    leader: UserId,
    leader_pub_key: PublicKey,
    // Only Some if leader
    private_key: Option<[u8; 32]>,
    base_key: [u8; 32],
    session_key: [u8; 32],
    name: String,
}

impl SimpleGroupSession {
    pub fn create_new(leader: UserId, gid: GroupId) -> Self {
        let key_pair = KeyPair::new(&mut rand::thread_rng());
        let pub_key = PublicKey {
            pub_key: key_pair.pub_key,
        };

        let mut s = Self::new(leader, gid, pub_key, rand::random());
        s.private_key = Some(key_pair.priv_key);

        s
    }

    pub fn new(
        leader: UserId,
        gid: GroupId,
        leader_pub_key: PublicKey,
        base_key: [u8; 32],
    ) -> Self {
        let mut secret = [0u8; 64];
        secret[..32].copy_from_slice(&leader_pub_key.pub_key);
        secret[32..].copy_from_slice(&base_key);

        let mut session_key = [0; 32];
        Salt::new(HKDF_SHA256, &[])
            .extract(&secret)
            .expand(&[b"PKOM INIT SIMPLE GROUP"], &CHACHA20_POLY1305)
            .unwrap()
            .fill(&mut session_key)
            .unwrap();

        let mut rng = Pcg64Mcg::new(gid.0.as_u128());
        let names = Petnames::medium();
        let name = names.generate(&mut rng, 3, "-");

        Self {
            gid,
            leader,
            leader_pub_key,
            private_key: None,
            base_key,
            session_key,
            name,
        }
    }

    pub fn prepare_invite_msg(&self) -> (SimpleGroupInvitationHeader, SimpleGroupInvitation) {
        (
            SimpleGroupInvitationHeader {
                gid: self.gid,
                leader: self.leader,
            },
            SimpleGroupInvitation {
                base_key: self.base_key,
                leader_pub_key: self.leader_pub_key,
            },
        )
    }

    pub async fn open_all(key: &[u8; 32], username: &str) -> Result<Vec<Self>> {
        let mut path = utils::data_dir(username).await?;
        path.push("sessions/groups");

        let mut v = Vec::new();
        fs::create_dir_all(&path).await?;
        let mut read_dir = fs::read_dir(path).await?;
        while let Some(e) = read_dir.next_entry().await? {
            if e.file_type().await?.is_file() {
                if let Some(uid) = e.file_name().to_str() {
                    v.push(Self::open(key, username, uid).await?);
                }
            }
        }

        Ok(v)
    }
}

#[async_trait]
impl Session for SimpleGroupSession {
    type Header = SimpleGroupMessageHeader;
    const IS_GROUP: bool = true;

    fn open_msg<'a>(
        &mut self,
        aad: &[u8],
        msg: &'a mut [u8],
        header: &Self::Header,
    ) -> Result<&'a mut [u8]> {
        let nonce = Nonce::assume_unique_for_key(header.nonce);
        let key = UnboundKey::new(&CHACHA20_POLY1305, &self.session_key).unwrap();

        let msg = LessSafeKey::new(key).open_in_place(nonce, Aad::from(aad), msg)?;

        Ok(msg)
    }

    fn seal_standard_msg(&mut self, mut msg: BytesMut, from: UserId) -> Result<Frame> {
        let nonce = rand::random();
        let header = SimpleGroupMessageHeader {
            nonce,
            timestamp: SystemTime::now()
                .duration_since(UNIX_EPOCH)
                .unwrap()
                .as_secs(),
            from,
            to: self.gid,
        };
        let header = rmp_serde::to_vec(&header)?;

        let nonce = Nonce::assume_unique_for_key(nonce);
        let key = UnboundKey::new(&CHACHA20_POLY1305, &self.session_key).unwrap();
        LessSafeKey::new(key).seal_in_place_append_tag(nonce, Aad::from(&header), &mut msg)?;

        Ok(libpkom::simple_group_message(header.into(), msg.freeze()))
    }

    fn seal_msg(&mut self, _msg: BytesMut, _header: Bytes) -> Result<Frame> {
        unimplemented!()
    }

    async fn open<'a>(
        enc_key: &'a [u8; 32],
        username: &'a str,
        session_name: &'a str,
    ) -> Result<Self> {
        let mut file = utils::open_file(username, "sessions/groups", session_name).await?;

        let mut buf = Vec::new();
        file.read_to_end(&mut buf).await?;

        let key = Salt::new(HKDF_SHA256, &[])
            .extract(enc_key.as_ref())
            .expand(&[b"PKOM ENCRYPT SIMPLE GROUP SESSION"], &CHACHA20_POLY1305)
            .unwrap()
            .into();

        let nonce = Nonce::try_assume_unique_for_key(&buf[..12]).unwrap();
        let buf = LessSafeKey::new(key).open_in_place(nonce, Aad::empty(), &mut buf[12..])?;

        Ok(rmp_serde::from_slice(&buf)?)
    }

    async fn save<'a>(
        &'a self,
        enc_key: &'a [u8; 32],
        username: &'a str,
        session_name: &'a str,
    ) -> Result<()> {
        let nonce = Nonce::assume_unique_for_key(rand::random());

        let mut buf = nonce.as_ref().to_vec();
        rmp_serde::encode::write(&mut buf, self).unwrap();

        let key = Salt::new(HKDF_SHA256, &[])
            .extract(enc_key.as_ref())
            .expand(&[b"PKOM ENCRYPT SIMPLE GROUP SESSION"], &CHACHA20_POLY1305)
            .unwrap()
            .into();

        let tag = LessSafeKey::new(key)
            .seal_in_place_separate_tag(nonce, Aad::empty(), &mut buf[12..])
            .unwrap();
        buf.extend_from_slice(tag.as_ref());

        let mut file = utils::create_file(username, "sessions/groups", session_name).await?;
        file.write_all(&buf).await?;

        Ok(())
    }

    fn id(&self) -> Id {
        Id::Group(self.gid)
    }

    fn name(&self) -> &str {
        &self.name
    }
}
