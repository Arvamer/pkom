use std::sync::Arc;

use anyhow::{Context, Error};
use cursive::{
    event::Event,
    theme::{Effect, Style},
    traits::{Nameable, Resizable, Scrollable},
    utils::markup::{markdown, StyledString},
    views::{
        BoxedView, Dialog, EditView, LinearLayout, Panel, ResizedView, SelectView, TextContent,
        TextView,
    },
    CbSink, Cursive,
};
use data_encoding::BASE64;
use tokio::{
    runtime::Handle,
    sync::{
        mpsc::{self, UnboundedSender},
        oneshot::{self, Sender},
    },
};
use tracing::{error, info};

use crate::{
    chat::{session::Session, ChatSession},
    simple_group::SimpleGroupSession,
    utils, State, UserId,
};
use libpkom::{Frame, GroupId, Id, KeyBundle};

pub struct Contact {
    pub chat: TextContent,
    pub name: String,
    pub id: Id,
}

struct RoomInfo {
    name: String,
    id: Id,
}

pub struct Rooms {
    rooms: Vec<Option<BoxedView>>,
    rooms_info: Vec<RoomInfo>,
    vacant: usize,
    username: String,
}

impl Rooms {
    fn new(contacts: Vec<Contact>, username: String) -> (Self, BoxedView) {
        let mut rooms = Vec::new();
        let mut rooms_info = Vec::new();
        for Contact { chat, name, id } in contacts {
            rooms.push(Some(Self::create_view(chat, id, &name, username.clone())));
            rooms_info.push(RoomInfo { name, id });
        }

        let first = rooms.first_mut().and_then(|o| o.take()).unwrap_or_else(|| {
            BoxedView::boxed(Panel::new(ResizedView::with_full_screen(TextView::new(
                "No one to talk :(",
            ))))
        });

        (
            Self {
                rooms,
                rooms_info,
                vacant: 0,
                username,
            },
            first,
        )
    }

    fn take_view(&mut self, idx: usize, old: BoxedView) -> BoxedView {
        self.rooms[self.vacant] = Some(old);
        self.vacant = idx;

        self.rooms[idx].take().unwrap()
    }

    pub fn push(&mut self, chat: TextContent, name: &str, id: Id) {
        info!("Added new room ({})", name);
        let view = Self::create_view(chat, id, name, self.username.clone());

        self.rooms.push(Some(view));
        self.rooms_info.push(RoomInfo {
            name: name.to_owned(),
            id,
        });
    }

    fn create_view(chat: TextContent, id: Id, other_name: &str, username: String) -> BoxedView {
        BoxedView::boxed(
            LinearLayout::vertical()
                .child(
                    Panel::new(ResizedView::with_full_screen(
                        TextView::new_with_content(chat.clone()).scrollable(),
                    ))
                    .title(other_name),
                )
                .child(Panel::new(
                    EditView::new()
                        .filler(" ")
                        .on_submit(move |siv, msg| {
                            let msg = msg.trim();
                            if !msg.is_empty() {
                                let mut ss = StyledString::plain("");
                                let style = Style {
                                    effects: Effect::Underline | Effect::Bold,
                                    color: None,
                                };
                                ss.append_styled(&username, style);
                                ss.append_styled(":", Style::from(Effect::Bold));
                                ss.append(" ");
                                ss.append(markdown::parse(msg));
                                ss.append("\n");
                                chat.append(ss);

                                if let Err(e) = siv
                                    .user_data::<Data>()
                                    .unwrap()
                                    .tx
                                    .send(Action::SendMsg(id, msg.to_owned()))
                                {
                                    error!("failed to send user input: {}", e);
                                }

                                siv.find_name::<EditView>("editor").unwrap().set_content("");
                            }
                        })
                        .with_name("editor"),
                )),
        )
    }

    pub fn is_current_group(&self) -> bool {
        !self.rooms_info.is_empty() && matches!(self.rooms_info[self.vacant].id, Id::Group(_))
    }

    pub fn current_id(&self) -> &Id {
        &self.rooms_info[self.vacant].id
    }
}

pub struct Data {
    handle: Handle,
    rooms: Rooms,
    state: Arc<State>,
    tx: UnboundedSender<Action>,
}

#[derive(Debug)]
pub enum Action {
    OpenedChatSession(ChatSession),
    OpenedSimpleGroupSession(SimpleGroupSession),
    InviteUser(UserId, GroupId, Sender<Result<(), Error>>),
    SendMsg(Id, String),
}

impl Data {
    pub fn push_room(&mut self, chat: TextContent, name: &str, id: Id) {
        self.rooms.push(chat, name, id);
    }

    pub fn rooms_len(&self) -> usize {
        self.rooms.rooms.len()
    }
}

#[derive(Copy, Clone, PartialOrd, PartialEq, Debug)]
pub enum ListData {
    Room(usize),
    AddUser,
    CreateSimpleGroup,
    InviteUser,
}

pub fn ui(contacts: Vec<Contact>, sink_tx: Sender<CbSink>, handle: Handle, state: Arc<State>) {
    let mut siv = Cursive::default();
    siv.set_autorefresh(true);
    sink_tx.send(siv.cb_sink().clone()).unwrap();

    siv.add_global_callback(Event::CtrlChar('q'), |s| s.quit());
    siv.add_global_callback(Event::CtrlChar('e'), |siv| {
        siv.focus_name("editor").unwrap();
    });
    siv.add_global_callback(Event::CtrlChar('p'), |siv| {
        let mut room_list = SelectView::new();

        let rooms = &siv.user_data::<Data>().unwrap().rooms;
        for (id, RoomInfo { name, .. }) in rooms.rooms_info.iter().enumerate() {
            room_list.add_item(name, ListData::Room(id));
        }

        room_list.add_item("Add user", ListData::AddUser);
        room_list.add_item("Create simple group", ListData::CreateSimpleGroup);

        if rooms.is_current_group() {
            room_list.add_item("Invite user to group", ListData::InviteUser);
        }

        room_list.set_autojump(true);
        room_list.set_on_submit(change_room);

        siv.add_layer(Dialog::around(room_list.scrollable()));
    });

    let (rooms, view) = Rooms::new(contacts, state.username.clone());

    let data = Data {
        handle,
        rooms,
        tx: state.action_tx.clone(),
        state,
    };
    siv.set_user_data(data);

    siv.add_fullscreen_layer(view);
    siv.run()
}

pub fn change_room(siv: &mut Cursive, data: &ListData) {
    match data {
        ListData::Room(idx) => {
            // Pop dialog with room switcher
            let upper = siv.pop_layer();
            let view = siv.pop_layer().or(upper).expect("no layer to pop");
            let view = siv
                .user_data::<Data>()
                .unwrap()
                .rooms
                .take_view(*idx, BoxedView::new(view))
                .unwrap();
            siv.add_fullscreen_layer(view);
        }
        ListData::AddUser => {
            siv.add_layer(
                Dialog::around(EditView::new().on_submit(add_user).fixed_width(20))
                    .title("Enter username")
                    .dismiss_button("Cancel")
                    .with_name("add user"),
            );
        }
        ListData::CreateSimpleGroup => {
            let (handle, state, tx) = {
                let data: &mut Data = siv.user_data().unwrap();
                (data.handle.clone(), data.state.clone(), data.tx.clone())
            };
            let siv_sink = siv.cb_sink().clone();

            handle.spawn(async move {
                let r: Result<_, Error> = try {
                    let gid = utils::send(&state.conn, &Frame::CreateSimpleGroup)
                        .await?
                        .response_gid()?;
                    let session = SimpleGroupSession::create_new(state.uid, gid);
                    session
                        .save(
                            &state.keys.read().await.enc_key.0,
                            &state.username,
                            &gid.0.to_hyphenated().to_string(),
                        )
                        .await?;
                    tx.send(Action::OpenedSimpleGroupSession(session)).unwrap();
                };

                match r {
                    Ok(()) => (),
                    Err(e) => {
                        error!("{:#}", e);
                        siv_sink
                            .send(Box::new(move |siv| {
                                siv.add_layer(Dialog::info(format!("{}", e)));
                            }))
                            .unwrap();
                    }
                }
            });
        }
        ListData::InviteUser => {
            let (uname_tx, mut uname_rx) = mpsc::unbounded_channel();
            siv.add_layer(
                Dialog::around(
                    EditView::new().on_submit(move |_, s| uname_tx.send(s.to_owned()).unwrap()),
                )
                .dismiss_button("cancel")
                .title("Enter username"),
            );
            let (handle, state, tx, gid) = {
                let data: &mut Data = siv.user_data().unwrap();
                let gid = GroupId::from_bytes(*data.rooms.current_id().as_bytes());

                (
                    data.handle.clone(),
                    data.state.clone(),
                    data.tx.clone(),
                    gid,
                )
            };
            let siv_sink = siv.cb_sink().clone();

            handle.spawn(async move {
                let r: Result<_, Error> = try {
                    let username = uname_rx.recv().await.context("operation cancelled")?;
                    let uid = utils::send(&state.conn, &libpkom::get_user_id(username))
                        .await?
                        .response_uid()?;
                    let (atx, arx) = oneshot::channel();
                    tx.send(Action::InviteUser(uid, gid, atx)).unwrap();
                    arx.await.unwrap()?;
                };

                match r {
                    Ok(()) => siv_sink
                        .send(Box::new(|siv| {
                            siv.pop_layer().unwrap();
                        }))
                        .unwrap(),
                    Err(e) => {
                        error!("{:#}", e);
                        siv_sink
                            .send(Box::new(move |siv| {
                                siv.add_layer(Dialog::info(format!("{}", e)));
                            }))
                            .unwrap();
                    }
                }
            });
        }
    }
}

fn add_user(siv: &mut Cursive, name: &str) {
    let conn = siv.user_data::<Data>().unwrap().state.conn.clone();
    let n = name.to_owned();
    let inner = async move {
        let uid = utils::send(&conn, &libpkom::get_user_id(n))
            .await
            .context("Requested user doesn't exist")?
            .response_uid()?;
        let key_bundle: KeyBundle = utils::send(&conn, &libpkom::get_pub_key_bundle(uid))
            .await
            .context("Failed to get key bundle")?
            .response_data()?;

        Ok::<_, Error>((uid, key_bundle))
    };

    let h = siv.user_data::<Data>().unwrap().handle.clone();
    let handle = h.clone();
    let n = name.to_owned();
    let siv_sink = siv.cb_sink().clone();
    h.spawn(async move {
        match inner.await {
            Ok((uid, key_bundle)) => {
                siv_sink
                    .send(Box::new(move |siv| {
                        let key_base64 = BASE64.encode(key_bundle.identity.pub_key.as_ref());
                        siv.add_layer(
                            Dialog::text(format!(
                                "Public key is {}. Please check it using some other secure channel",
                                key_base64
                            ))
                            .button("Continue", move |siv| {
                                create_session(&handle, n.clone(), uid, key_bundle.clone(), siv)
                            })
                            .dismiss_button("Cancel"),
                        );
                    }))
                    .unwrap();
            }
            Err(e) => {
                error!("{:#}", e);
                siv_sink
                    .send(Box::new(move |siv| {
                        siv.add_layer(Dialog::info(format!("{}", e)));
                    }))
                    .unwrap();
            }
        }
    });
}

fn create_session(
    handle: &Handle,
    name: String,
    uid: UserId,
    key_bundle: KeyBundle,
    siv: &mut Cursive,
) {
    // Dismiss confirmation dialog
    siv.pop_layer().unwrap();
    // Dismiss add new dialog
    siv.pop_layer().unwrap();

    let state = siv.user_data::<Data>().unwrap().state.clone();
    let siv_sink = siv.cb_sink().clone();
    handle.spawn(async move {
        let alice =
            ChatSession::new_alice(&state.keys.read().await.identity_key, key_bundle, name, uid);
        match alice {
            Ok(session) => siv_sink
                .send(Box::new(move |siv| {
                    siv.user_data::<Data>()
                        .unwrap()
                        .tx
                        .send(Action::OpenedChatSession(session))
                        .unwrap();
                }))
                .unwrap(),
            Err(e) => {
                error!("{:#}", e);
                siv_sink
                    .send(Box::new(move |siv| {
                        siv.add_layer(Dialog::info(format!("{}", e)));
                    }))
                    .unwrap();
            }
        }
    });
}
