use libpkom::{MessageHeader, SimpleGroupMessageHeader, UserId};

pub trait Header {
    fn from(&self) -> UserId;
}

impl Header for MessageHeader {
    fn from(&self) -> UserId {
        self.from
    }
}

impl Header for SimpleGroupMessageHeader {
    fn from(&self) -> UserId {
        self.from
    }
}
