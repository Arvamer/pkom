use crate::State;
use anyhow::Result;
use async_trait::async_trait;
use bytes::{Bytes, BytesMut};
use libpkom::{Frame, Id, UserId};

#[async_trait]
pub trait Session {
    type Header;
    const REQUIRE_SAVE_AFTER_ENC: bool = false;
    const IS_GROUP: bool = false;

    fn open_msg<'a>(
        &mut self,
        aad: &[u8],
        msg: &'a mut [u8],
        nonce: &Self::Header,
    ) -> Result<&'a mut [u8]>;

    fn seal_standard_msg(&mut self, msg: BytesMut, from: UserId) -> Result<Frame>;
    fn seal_msg(&mut self, msg: BytesMut, header: Bytes) -> Result<Frame>;

    async fn open<'a>(
        enc_key: &'a [u8; 32],
        username: &'a str,
        session_name: &'a str,
    ) -> Result<Self>
    where
        Self: Sized;

    async fn save<'a>(
        &'a self,
        enc_key: &'a [u8; 32],
        username: &'a str,
        session_name: &'a str,
    ) -> Result<()>;

    fn id(&self) -> Id;
    fn id_bytes(&self) -> [u8; 16] {
        *self.id().as_bytes()
    }

    fn name(&self) -> &str {
        "???"
    }

    /// Returns true if was special msg
    fn handle_special_msg(
        &mut self,
        _state: &State,
        _body: &[u8],
        _header: &Self::Header,
    ) -> Result<bool> {
        Ok(false)
    }
}
